package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：订单实体类
 */

@Data
public class Order {

    private Integer autoId;    //订单编号
    private String orderCode;      //商品一级分类编号
    private String productFirstTypeCode;     //商品一级分类名称
    private String productFirstTypeName;     //商品二级分类编号
    private String productSecondTypeCode;    //商品二级分类名称
    private String productSecondTypeName;    //商品三级分类编号
    private String productThirdTypeCode;     //商品三级分类名称
    private String productThirdTypeName;     //商品编号
    private String productCode;    //商品编号
    private String productName;    //商品名称
    private String productSpecifica;   //商品规格
    private String platformPrice;  //平台价格
    private String supermarketPrice;   //超市价格
    private String copiesNumber;   //份数
    private String orderState;     //状态 1-下单成功2-商家备货中 3-物流配送中 4-自提点待提货 5-已提货6-待评价
    private String selfRaisingPointCode;     //自提点编号
    private String selfRaisingPointName;     //自提点名称
    private String selfRaisingPointPhone;    //自提点电话
    private String sellerCode;     //卖家编号
    private String sellerName;     //卖家名称
    private String createDate;     //创建时间
    private String createUser;     //创建人
    private String ext1;        //扩展字段1
    private String ext2;        //扩展字段2
    private String ext3;        //扩展字段3
    private String ext4;        //扩展字段4
    private String ext5;        //扩展字段5

}
