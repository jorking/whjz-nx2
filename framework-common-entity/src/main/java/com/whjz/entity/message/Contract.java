package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-10
 * remarks：合同实体类
 */

@Data
public class Contract {
    private Integer autoId;     //自增ID
    private String contractCode;   //合同编号
    private String contractName;   //合同名称
    private String contractType;   //合同类型1-供应商合同 2-物流合同 3-驿站合同
    private String contractFirstParty;    //合同甲方
    private String contractSecondParty;   //合同乙方
    private String contractDesc;   //合同概况
    private String dueDate;    //合同到期日
    private String signDate;   //合同签订日期
    private String contractState;  //合同期限1、正常2、合同已超期
    private String signPlace;  //签订地点
    private String contractFile;   //合同文件
    private String createUser; //合同录入人
    private String createDate; //合同创建时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
