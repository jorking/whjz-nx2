package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-12
 *remarks:银行卡管理
 */
@Data
public class BankCard {
    /**
     * 序号
     */
    private int autoId;
    /**
     *开户银行
     */
    private String openBack;
    /**
     *开户网点
     */
    private String  openBackAddress;
    /**
     *卡号码
     */
    private String bankCardNumber;
    /**
     *户名
     */
    private String bankCardName;
    /**
     *预留手机号
     */
    private String bankCardPhone ;
    /**
     *創建人
     */
    private  String createUser;
    /**
     *创建时间
     */
    private  String createDate ;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;

}
