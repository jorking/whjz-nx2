package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-1013
 * remarks;配载单管理
 */
@Data
public class Stowage {
    /**
     * 序号
     */
    private int autoId ;
    /**
     *运载单编号
     */
    private String  stowageCode;
    /**
     *司机编号
     */
    private String driverCode;
    /**
     *司机名称
     */
    private String  driverName;
    /**
     *驾驶证编号
     */
    private String driverLicense ;
    /**
     *车牌号
     */
    private String  licenseNumber;
    /**
     *承载量
     */
    private String bearingCapacity;
    /**
     *班次
     */
    private String frequency;
    /**
     *合计份数
     */
    private String sumCopiesNumber ;
    /**
     *配载单流水号
     */
    private String serialNumber;
    /**
     *状态 1-商家备货中2-司机送货中3-驿站已签收
     */
    private String stowageState;
    /**
     *创建人
     */
    private String createUser;
    /**
     *创建时间
     */
    private String createDate;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;


}
