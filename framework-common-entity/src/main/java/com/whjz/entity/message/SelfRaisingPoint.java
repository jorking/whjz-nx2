package com.whjz.entity.message;


import lombok.Data;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：自提点实体类
 */

@Data
public class SelfRaisingPoint {

    private Integer autoId;    //自增ID
    private String code;    //自提点编号
    private String name;    //自提点名称
    private String address; //自提点地址
    private String phone;   //自提点电话
    private String chargePerson;   //自提点负责人
    private String chargePersonPhone; //自提点负责人电话
    private String alternateContact;   //自提点备用联系人
    private String alternateContactPhone;     //自提点备用联系人电话
    private String businessLicense;    //自提点营业执照信息
    private String legalPersonName;   //自提点法人姓名
    private String legalPersonPhone;  //自提点法人电话
    private String bankDeposit;    //开户行
    private String accountOpeningBranch;  //开户网点
    private String accountName;    //户名
    private String accountNumber;  //账号
    private String createDate; //创建时间
    private String createUser;     //创建人
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5

}
