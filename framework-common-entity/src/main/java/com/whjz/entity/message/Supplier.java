package com.whjz.entity.message;

/**
 * author:hj
 * date:2020-10-10
 * remarks:供应商类
 */
import lombok.Data;

@Data
public class Supplier {
    /**
     * 序号
     */
    private  int autoId;
    /**
     *供应商编码
     */
    private String supplierCode;
    /**
     *企业类型
     */
    private String enterpriseType;
    /**
     *供应商名称
     */
    private String supplierName;
    /**
     *经营范围
     */
    private String businessScope;
    /**
     *注册资本
     */
    private String registerCapital;
    /**
     *注册日期
     */
    private String registerDate;
    /**
     *营业期限
     */
    private String businessTerm;

    /**
     *供应商概况
     */
    private String supplierGeneralization;
    /**
     *供应商地址
     */
    private String supplierAddress;
    /**
     *供应商电话
     */
    private String supplierPhone;
    /**
     *供应商法人姓名
     */
    private String supplierLegalpersonName;
    /**
     *供应商法人电话
     */
    private String supplierLegalpersonPhone;
    /**
     *供应商登录账号
     */
    private String supplierLoginNumber;
    /**
     *供应商传真
     */
    private String supplierFax;
    /**
     *供应商邮箱
     */
    private String supplierEmail;
    /**
     *供应商负责人姓名
     */
    private String supplierChargepersonName;
    /**
     *负责人电话
     */
    private String supplierChargepersonPhone;
    /**
     *主要产品
     */
    private String mainProducts;
    /**
     *供应能力
     */
    private String supplyCapacity;
    /**
     *发展潜力
     */
    private String developmentPotential;
    /**
     *企业规模
     */
    private String enterpriseScale;
    /**
     *供应商开户银行
     */
    private String supplierBank;
    /**
     *供应商开户账号
     */
    private String supplierAccountNumber;
    /**
     *供应商税号
     */
    private String supplierTaxNumber;
    /**
     *供应商营业执照
     */
    private String businessLicense;
    /**
     *营业执照到期日
     */
    private String certificateExpirationDueDate;
    /**
     *生产许可证
     */
    private String productionLicense;
    /**
     *生产许可证到期日
     */
    private String  productionLicenseDueDate;
    /**
     *质量体系证书
     */
    private String qualitySystemCertificate;
    /**
     *质量体系证书证件到期日
     */
    private String qualitySystemCertificateDueDate;
    /**
     *产品标识
     */
    private String productIdentification;
    /**
     *行业检测报告
     */
    private String industryMonitoringReport;
    /**
     *产品编码
     */
    private String productCode;
    /**
     *合约到期日
     */
    private String contractExpirationDate;
    /**
     *供应商等级
     */
    private String supplierLevel;
    /**
     *供应商账期
     */
    private String supplierAccountingPeriod;
    /**
     *供应商评分
     */
    private String supplierRating;
    /**
     *供应商质量工程师
     */
    private String supplierQualityEngineer;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private String ext5;
    /**
     *扩展字段6
     */
    private String ext6;
    /**
     *扩展字段7
     */
    private String ext7;
    /**
     *扩展字段8
     */
    private String ext8;
    /**
     *扩展字段9
     */
    private String ext9;

}
