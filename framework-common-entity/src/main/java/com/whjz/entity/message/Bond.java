package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-9
 * remarks:供应商保证金类
 */
@Data
public class Bond {
    /**
     * 序号
     */

   private Integer autoId;
    /**
     *供应商编号
     */
    private String supplierCode;
    /**
     *保证金模板编号
     */
    private String bondStandardCode;
    /**
     *保证金状态（1-正常 2-暂停）
     */
    private String bondState;
    /**
     *缴纳日期
     */
    private String paymentDate;
    /**
     *缴纳人
     */
    private String paymentUser;
    /**
     * 收款人
     *
     */
    private String payee;
    /**
     *支付渠道(1-微信 2-支付宝 3-转账)
     */
    private String paymentChannels;
    /**
     *支付凭证
     */
    private String paymentVoucher;
    /**
     *创建人
     */
    private String createUser;
    /**
     *创建时间
     */
    private String createDate;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;





}
