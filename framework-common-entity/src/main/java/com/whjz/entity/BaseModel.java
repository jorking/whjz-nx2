package com.whjz.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@Data
public class BaseModel implements Serializable {

	private static final long serialVersionUID = 7772925508479870706L;

	/**
	 * 自增主键
	 */
	@ApiModelProperty(value = "自增主键")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, name = "auto_id", columnDefinition = "int(11) NOT NULL COMMENT '自增Id'")
	private Integer autoId;

	@ApiModelProperty(value = "创建时间")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '创建时间'")
	private String createDate;

	@ApiModelProperty(value = "更新时间")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '更新时间'")
	private String updateDate;
	
	@ApiModelProperty(value = "扩展字段1")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '扩展字段1'")
	private String ext1;
	
	@ApiModelProperty(value = "扩展字段2")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '扩展字段2'")
	private String ext2;
	
	@ApiModelProperty(value = "扩展字段3")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '扩展字段3'")
	private String ext3;
	
	@ApiModelProperty(value = "扩展字段4")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '扩展字段4'")
	private String ext4;
	
	@ApiModelProperty(value = "扩展字段5")
	@Column(columnDefinition = "VARCHAR(50) DEFAULT NULL COMMENT '扩展字段5'")
	private String ext5;

}
