package com.whjz.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 配置对象
 *
 */
@Data
public class Config extends BaseModel implements Serializable {

	private static final long serialVersionUID = 8759596465831839829L;

	/**
	 * 配置ID
	 */
	private String configId;

	/**
	 * 配置编号
	 */
	private String configCode;

	/**
	 * 配置名称
	 */
	private String configName;

	/**
	 * 配置值
	 */
	private String configValue;

	/**
	 * 状态
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 删除标识
	 */
	private String delFlag;

}