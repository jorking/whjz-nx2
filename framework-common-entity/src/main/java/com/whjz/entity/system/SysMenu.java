package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-24
 * remarks 菜单实体类
 */

@Data
public class SysMenu {
    private Integer autoId;    //自增Id
    private String parentId;   //父菜单Id
    private String menuCode;   //菜单编码
    private String menuLogo;   //菜单图标
    private String menuName;   //菜单名称
    private String menuSeq;    //菜单序号
    private String menuUrl;    //菜单地址
    private String operator;    //操作人
    private String createUser; //创建人
    private String state;   //菜单状态,1--启用，2--停用
    private String createDate; //创建时间
    private String updateDate; //更新时间
    private String ext1;    //菜单类别 1-pad菜单，2--后管菜单
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5

}
