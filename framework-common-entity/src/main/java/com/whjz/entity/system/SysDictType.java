package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-21
 * remarks：字典类别实体类
 */

@Data
public class SysDictType {
    private Integer autoId; //自增Id
    private String dictTypeId;  //字典类型id
    private String dictTypeCode;    //字典类型编码
    private String dictTypeName;    //字典类型名称
    private String state;  //状态 1-启用，2--停用
    private String createDate;  //创建时间
    private String updateDate;  //更新时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
