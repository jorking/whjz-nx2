package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-25
 * remarks 登录日志实体类
 */

@Data
public class SysIbsaccessLogForLogin {
    private Integer autoId;    //自增Id
    private String userId;     //用户编号
    private String orgId;  //用户机构编号
    private String address;     //地址
    private String bankType;   //银行类型
    private String clientNo;   //客户编号
    private String deviceId;   //设备编号
    private String deviceType; //设备类型
    private String loginState; //登录状态 1-成功 2-失败 3-首次登陆
    private String latitude;    //纬度
    private String longitude;   //经度
    private String loginDate;  //登录时间
    private String logoutDate; //登出时间
    private String network; //网络
    private String pushDeviceId;  //推送设备id
    private String recordId;   //记录id
    private String version; //版本号
    private String phoneModel; //手机型号
    private String wifiName;   //wifi名
    private String imgSeq; //影像流水
    private String similarity;  //相似度
    private String failReason; //失败原因
    private String remarks1;    //备注
    private String createDate; //创建时间
    private String updateDate; //修改时间
    private String ext1;    //登录来源 2-移动端 3-后台管理
    private String ext2;    //登录方式，传统(密码登录)   人脸登录
    private String ext3;    //token信息
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
    private String ext6;    //扩展字段6
    private String ext7;    //扩展字段7
    private String ext8;    //扩展字段8
    private String ext9;    //扩展字段9
}
