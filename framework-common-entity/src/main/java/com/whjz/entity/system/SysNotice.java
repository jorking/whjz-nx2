package com.whjz.entity.system;


import lombok.Data;

/**
 * author: fln
 * date: 2020-09-29
 * remarks 通知公告实体类
 */

@Data
public class SysNotice {
    private Integer autoId;     //自增ID
    private String noticeCode;     //通知公告编号
    private String noticeTitle;    //通知公告标题
    private String noticeContent;  //通知内容
    private String noticeType; //通知类别 1-角色级 2-账号级 3-平台级
    private String noticeRange;    //通知范围 1角色级：下属角色 2账号级，针对账号账号3-平台级针对平台
    private String noticeState;    //通知状态 1-启用，2-停用
    private String noticeImportance;   //通知重要程度 1-正常 2-紧急
    private String startDate;  //开始时间
    private String endDate;    //结束时间
    private String createUser;     //创建人
    private String ext1;    //扩展字段1
    private String ext2;     //扩展字段1
    private String ext3;     //扩展字段1
    private String ext4;     //扩展字段1
    private String ext5;     //扩展字段1
}
