package com.whjz.entity.system;


import lombok.Data;

/**
 * author: fln
 * date: 2020-09-23
 * remarks 在线用户实体类
 */

@Data
public class SysUserOnline {
    private String sessionId;   //用户会话id
    private String loginName;   //登录账号
    private String deptName;    //部门名称
    private String ipaddr;  //登录IP地址
    private String loginLocation;   //登录地点
    private String browser; //浏览器类型
    private String os;  //操作系统
    private String states;  //在线状态on_line在线off_line离线
    private String startTimesTamp;  //session创建时间
    private String lastAccessTime;  //session最后访问时间
    private Integer expireTime; //超时时间，单位为分钟
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
