package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-20
 * remarks：角色实体类
 */

@Data
public class SysRole {
    private Integer autoId;  //自增Id
    private String deptId;  //创建机构
    private String roleCode;    //角色编号（岗位编号 用户自己输入）
    private String roleName;    //角色名称
    private String roleDesc;    //角色描述
    private String roleState;   //角色状态,1-启用,2-停用
    private String roleType;    //角色类型
    private String createUser;  //创建人
    private String updateUser;  //操作人
    private String createDate;  //创建时间
    private String updateDate;  //更新时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
