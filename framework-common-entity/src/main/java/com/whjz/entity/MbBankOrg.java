package com.whjz.entity;

import java.util.List;

public class MbBankOrg {
    private Integer autoId;

    private String createDate;

    private String updateDate;

    private Integer orgId;

    private String orgCode;

    private String orgName;

    private Integer parentOrgId;

    private String shortName;

    private String bankNum;

    private String orgLevel;

    private String orgState;

    private String orgType;

    private String contUser;

    private String phonrNum;

    private String address;

    private String postCode;

    private String email;

    private String remark;

    private String creator;

    private String modifier;

    private String innerCode;

    private String ext01;

    private String ext02;

    private String ext03;

    private String ext04;

    private String ext05;

    private String ext06;

    private String ext07;

    private String ext08;

    private String ext09;

    private String parentOrgName;
    private String parentOrgCode;
    private List<MbBankOrg> childOrg;

    public String getParentOrgName() {
        return parentOrgName;
    }

    public void setParentOrgName(String parentOrgName) {
        this.parentOrgName = parentOrgName;
    }

    public String getParentOrgCode() {
        return parentOrgCode;
    }

    public void setParentOrgCode(String parentOrgCode) {
        this.parentOrgCode = parentOrgCode;
    }

    public List<MbBankOrg> getChildOrg() {
        return childOrg;
    }

    public void setChildOrg(List<MbBankOrg> childOrg) {
        this.childOrg = childOrg;
    }

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public Integer getParentOrgId() {
        return parentOrgId;
    }

    public void setParentOrgId(Integer parentOrgId) {
        this.parentOrgId = parentOrgId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName == null ? null : shortName.trim();
    }

    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum == null ? null : bankNum.trim();
    }

    public String getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel == null ? null : orgLevel.trim();
    }

    public String getOrgState() {
        return orgState;
    }

    public void setOrgState(String orgState) {
        this.orgState = orgState == null ? null : orgState.trim();
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType == null ? null : orgType.trim();
    }

    public String getContUser() {
        return contUser;
    }

    public void setContUser(String contUser) {
        this.contUser = contUser == null ? null : contUser.trim();
    }

    public String getPhonrNum() {
        return phonrNum;
    }

    public void setPhonrNum(String phonrNum) {
        this.phonrNum = phonrNum == null ? null : phonrNum.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode == null ? null : postCode.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public String getInnerCode() {
        return innerCode;
    }

    public void setInnerCode(String innerCode) {
        this.innerCode = innerCode == null ? null : innerCode.trim();
    }

    public String getExt01() {
        return ext01;
    }

    public void setExt01(String ext01) {
        this.ext01 = ext01 == null ? null : ext01.trim();
    }

    public String getExt02() {
        return ext02;
    }

    public void setExt02(String ext02) {
        this.ext02 = ext02 == null ? null : ext02.trim();
    }

    public String getExt03() {
        return ext03;
    }

    public void setExt03(String ext03) {
        this.ext03 = ext03 == null ? null : ext03.trim();
    }

    public String getExt04() {
        return ext04;
    }

    public void setExt04(String ext04) {
        this.ext04 = ext04 == null ? null : ext04.trim();
    }

    public String getExt05() {
        return ext05;
    }

    public void setExt05(String ext05) {
        this.ext05 = ext05 == null ? null : ext05.trim();
    }

    public String getExt06() {
        return ext06;
    }

    public void setExt06(String ext06) {
        this.ext06 = ext06 == null ? null : ext06.trim();
    }

    public String getExt07() {
        return ext07;
    }

    public void setExt07(String ext07) {
        this.ext07 = ext07 == null ? null : ext07.trim();
    }

    public String getExt08() {
        return ext08;
    }

    public void setExt08(String ext08) {
        this.ext08 = ext08 == null ? null : ext08.trim();
    }

    public String getExt09() {
        return ext09;
    }

    public void setExt09(String ext09) {
        this.ext09 = ext09 == null ? null : ext09.trim();
    }
}