package com.whjz.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserParam implements Serializable {

    private static final long serialVersionUID = -6931931673983801853L;
    //客户号
    private String clientSeqNo;
    //token
    private String token;
    //设备号
    private String deviceId;

    private String custNo;

    private int source;
    private String sourceType;
    private String userId2;
}
