package com.whjz.common.service.impl;

import com.whjz.common.dao.MbUserLocationMapper;
import com.whjz.common.service.UserLocationService;
import com.whjz.entity.MbUserLocation;
import com.whjz.service.impl.AbstractServiceImpl;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Description TODO
 * @Author kangxb
 * @ClassName UserLocationServiceImpl
 * @Date 2019/10/8
 **/
@Service
@Slf4j
public class UserLocationServiceImpl extends AbstractServiceImpl implements UserLocationService {
    @SuppressWarnings("all")
    @Autowired
    MbUserLocationMapper mbUserLocationMapper;

    /**
     * 查询定位信息
     * @param req
     * @return
     */
    @Override
    public Map queryList(Map<String, String> req) {
        LogInfo.serviceImplLogInfo("查询定位信息");
        Integer count = null;
        String dvLableId = req.get("dvLableId");// 设备号
        String vUser = req.get("vUser");// 虚拟柜员号

        if ("true".equals(req.get("isPage"))) {
            PageUtil.startPage(req);
            count = mbUserLocationMapper.countList(req);
        }
        List<MbUserLocation> list = mbUserLocationMapper.queryList(req);
        return success(PageUtil.getPageMap(req, list, count));
    }

    /**
     * 插入定位信息
     * @param req
     * @return
     */
    @Override
    public Map saveUserLocation(Map<String, String> req) {
        try {
            int updateCount = 0;
            MbUserLocation mbUserLocation = Utils.getParamDTO(req, MbUserLocation.class);
            mbUserLocation.setDvMasterId(StringUtils.getString(req.get("deviceId")));
            mbUserLocation.setCreateDate(DateUtils.getCurDate(DateUtils.yyyy_MM_dd_HH_mm_ss_EN));
            mbUserLocation.setUpdateDate(DateUtils.getCurDate(DateUtils.yyyy_MM_dd_HH_mm_ss_EN));
            updateCount = mbUserLocationMapper.insertSelective(mbUserLocation);
            return  ResultUtil.resultMap(ResultCode.RESULT_CODE_SUCCESS.desc(), ResultCode.RESULT_CODE_SUCCESS.code(), updateCount);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            if (e.getMessage().contains("Data too long for column")) {
                return fail(ResultCode.RESUTL_CODE_FAILURE.code(), "字段过长，请检查入参");
            }
            return fail(ResultCode.RESUTL_CODE_FAILURE.code(), "插入信息失败");
        }
    }

    /**
     * 根据条件查询所有信息
     * @param req
     * @return
     */
    @Override
    public Map queryAllList(Map<String, String> req) {

        List<MbUserLocation> list = mbUserLocationMapper.queryAllList(req);

        return success(list);
    }
}
