package com.whjz.common.dao;

import com.whjz.common.entity.DictDTO;
import com.whjz.entity.Config;

import java.util.List;
import java.util.Map;

public interface CommonMapper {
	//获取配置列表
    List<Config> getListConfig();
    //获取字典集合
    List<DictDTO> getListDict();

    List<DictDTO> queryDictByKey(Map req);
}