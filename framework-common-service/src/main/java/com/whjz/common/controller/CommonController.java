package com.whjz.common.controller;

import com.whjz.common.service.*;
import com.whjz.common.service.CommonService;
import com.whjz.common.service.MbCommonService;
import com.whjz.common.service.UserLocationService;
import com.whjz.redis.RedisUtils;
import com.whjz.service.impl.AbstractControllerImpl;
import com.whjz.utils.CodeUtil;
import com.whjz.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 公共模块
 * @author liweibin
 *
 */
@RestController
@RequestMapping("common")
@Slf4j
public class CommonController extends AbstractControllerImpl {

	@Autowired
	private CommonService commonService;

	@Autowired
	public UserLocationService userLocationService;

	@Autowired
	private MbCommonService mbCommonService;
    /**
	 * 获取字典集合
	 */
    @PostMapping(value = "/dict")
    @ApiOperation(value = "字典接口", notes = "字典接口")
	public Map<String,Object> getListDict(@RequestBody Map<String,String> reqBody) {
    	return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), commonService.getListDict());
	}
    
    /**
	 * 获取配置列表（内部调用）
	 */
    @PostMapping(value = "/config")
    @ApiOperation(value = "配置接口", notes = "获取一些配置信息接口")
	public Map<String,Object> getConfig() {
		return commonService.getListConfig();
	}
    
    /**
	 * 字典集合重置
	 */
    @PostMapping(value = "/removeDict")
    @ApiOperation(value = "字典接口", notes = "字典接口")
	public Map<String,Object> removeDict() {
    	RedisUtils.deleteByKey("dict");
    	return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
	}
    
    @GetMapping("clearCache")
    @ApiOperation(value="清除redis缓存清除字典",notes="清除缓存")
    public Map<String,Object> clearCache(){
    	RedisUtils.deleteByKey("dict");
    	RedisUtils.deleteByKey("config");

    	return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);

    }
	@PostMapping(value = "/queryDictByCode")
	@ApiOperation(value = "通过code查询字典项", notes = "通过code查询字典项")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "code", value = "字典key", paramType = "query"),
	})
	public Map<String,Object> queryDictByKey(@RequestBody Map<String,String> reqBody) {
		return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), commonService.queryDictByKey(reqBody));
	}


	@ResponseBody
	@PostMapping("/queryTheBackReqNO")
	@ApiOperation(value = "请求最大后台流水",notes="请求最大后台流水")
	public String queryTheBackReqNO(@RequestBody  Map<String, String> reqBody){
    	return commonService.queryTheMaxBackReqNO(reqBody);
	}

	@ResponseBody
	@PostMapping("/checkTheUserPasswd")
	@ApiOperation(value = "校验用户密码",notes = "校验用户密码")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custNo",value = "用户号",paramType = "query"),
			@ApiImplicitParam(name = "userPasswd",value = "用户密码",paramType = "query")
	})
	public Map<String,Object> checkTheUserPasswd(@RequestBody Map<String ,String >reqBody){
    	return commonService.checkTheUserPasswd(reqBody);
	}


	/**
	 * 重置密码
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/resetPasswd")
	@ApiOperation(value = "修改密码",notes = "修改密码")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custNo",value = "用户号",paramType = "query"),
			@ApiImplicitParam(name = "userPasswd",value = "用户密码",paramType = "query")
	})
	Map<String,Object> resetPasswd(@RequestBody Map<String,String > req){
		return commonService.resetPasswd(req);
	}

	/**
	 * 设置默认密码
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/resetDefaultPasswd")
	@ApiOperation(value = "恢复默认密码",notes = "恢复默认密码")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId",value = "需要重置密码的柜员编号",paramType = "query",required = true),
			@ApiImplicitParam(name = "phoneEightPassw", value = "加密后的手机号后8为", paramType = "query", required = true),
			@ApiImplicitParam(name = "sourceType",value = "登录渠道（2-工作站平板  3-后管）",paramType = "query",required = true),
			@ApiImplicitParam(name = "custNo",value = "当前登录柜员编号",paramType = "query"),

	})
	Map<String,Object> resetDefaultPasswd(@RequestBody Map<String,String > req){
		checkNotNullOrEmpty(req,"userId");
		return commonService.resetDefaultPasswd(req);
	}

	/**
	 * 检查业务请求权限
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/countBusCodeMenu")
	@ApiOperation(value = "检查业务请求权限",notes = "检查业务请求权限")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId",value = "用户编号",paramType = "query",required = true),
			@ApiImplicitParam(name = "busCode", value = "业务编号", paramType = "query", required = true),

	})
	Map<String,Object> countBusCodeMenu(@RequestBody Map<String,String> req){
		checkNotNullOrEmpty(req,"userId","busCode");
		return mbCommonService.countBusCodeMenu(req);
	}

	/**
	 * 登录
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/login")
	@ApiOperation(value = "后管与pad公共登录",notes = "后管与pad公共登录")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custNo",value = "用户号",paramType = "query",required = true),
			@ApiImplicitParam(name = "userPasswd",value = "密码",paramType = "query",required = true),
			@ApiImplicitParam(name = "sourceType",value = "类型 2-app 3-后管",paramType = "query",required = true),
			@ApiImplicitParam(name = "noteNo",value = "短信验证码(调workstation中sendNoteByMaster登录发送短信接口，会收到（noteNo,uuid）)",paramType = "query",required = true),
			@ApiImplicitParam(name = "uuid",value = "短信随机数",paramType = "query",required = true),
	})
	public Map<String,Object> loginCommon(@RequestBody  Map<String,String > req){
		return commonService.login(req);
	}


	@ApiOperation(value = "插入用户所有位置列表信息", notes = "插入用户所有位置列表信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "dvMasterId", value = "设备编号", paramType = "query"),
			@ApiImplicitParam(name = "userId", value = "登录柜员号", paramType = "query"),
			@ApiImplicitParam(name = "orgId", value = "机构表编号", paramType = "query"),
			@ApiImplicitParam(name = "longitude", value = "经度", paramType = "query"),
			@ApiImplicitParam(name = "latitude", value = "纬度", paramType = "query"),
			@ApiImplicitParam(name = "address", value = "地址", paramType = "query"),
			@ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
			@ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
			@ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
			@ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
	})

	@RequestMapping(value = "/saveUserLocation", method = RequestMethod.POST)
	public Map saveUserLocation(@RequestBody Map<String, String> req)  {
		return userLocationService.saveUserLocation(req);
	}




	@ResponseBody
	@PostMapping("/selectUserInfoByUserId")
	@ApiOperation(value = "根据用户编号查询用户信息",notes = "根据用户编号查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId",value = "柜员编号",paramType = "query",required = true),
	})
	Map selectUserInfoByUserId(@RequestBody  Map<String,String > req){
		return commonService.selectUserInfoByUserId(req);
	}

	@ResponseBody
	@PostMapping(value = "/loginOut")
	@ApiOperation(value = "安全退出", notes = "安全退出")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custNo",value = "柜员号（pad端和后管退出登录）",paramType = "query",required = true)
	})
	public Map<String, Object> loginOut(@RequestBody Map<String, String> reqBody) {
		return commonService.loginOut(reqBody);
	}

}
