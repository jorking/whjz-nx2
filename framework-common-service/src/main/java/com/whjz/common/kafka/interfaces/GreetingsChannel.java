package com.whjz.common.kafka.interfaces;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author youkun
 * @data 2018/7/11
 *    kafa的消息定义
 */
public interface GreetingsChannel {

    String INPUT = "greetings-in";

    String OUTPUT = "greetings-out";

    /**
     * 接受消息
     * @return
     */
    @Input(INPUT)
    SubscribableChannel inboundGreetings();

    /**
     * 发送消息
     * @return
     */
    @Output(OUTPUT)
    MessageChannel outboundGreetings();
}
