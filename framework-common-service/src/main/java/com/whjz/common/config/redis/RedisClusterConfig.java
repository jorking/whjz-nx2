package com.whjz.common.config.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * redis 集群的配置
 */
@Configuration
public class RedisClusterConfig {

    @Component
    @ConfigurationProperties(prefix = "spring.redis.cluster")
    @Data
    @RefreshScope
    public class ClusterConfigurationProperties{
        List<String> nodes;
    }

    @Bean
    @RefreshScope
    @Primary
    public RedisConnectionFactory connectionFactory(ClusterConfigurationProperties clusterConfigurationProperties){
        return  new JedisConnectionFactory(new RedisClusterConfiguration(clusterConfigurationProperties.getNodes()));
    }
}
