package com.whjz.common.aop;

import com.whjz.utils.UserInfoUtil;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @Description todo
 * @Author lcc
 * @Date 2019/4/29 11:11
 **/
@Aspect
@Component
public class WebLogAspect {

    @Pointcut("execution(public * com.whjz.common.controller.*.*(..))")
    public void webLog(){

    }
   /* @Before("webLog()")
    public void before(JoinPoint joinPoint) throws  Throwable{
        try {
            Object[] args =  joinPoint.getArgs();
            if(args != null && args.length > 0 ){
                Object obj = args[0];
                if(obj instanceof  Map) {
                    Map map = (Map)obj;
                    if(map.get("custNo") != null ) {
                        MDC.put("custNo", map.get("custNo").toString());
                        try{
                            MbBackUser mbBackUser= UserInfoUtil.getLocalUserInfo(map.get("custNo").toString(),map.get("sourceType").toString());
                            MDC.put("orgId", mbBackUser.getOrgId());
                        }catch (Exception e){
                        }
                    }
                    if(map.get("userId") != null ) {
                        MDC.put("custNo", map.get("userId").toString());
                    }
                    if(map.get("logseq") != null ) {
                        MDC.put("logseq", map.get("logseq").toString());
                    }
                }else if(obj instanceof  String) {
                    JSONObject json = JSONObject.parseObject(obj.toString());
                    if (json.containsKey("custNo")) {
                        MDC.put("custNo", json.get("custNo").toString());
                    }
                    if (json.containsKey("userId")) {
                        MDC.put("custNo", json.get("userId").toString());
                    }
                    if (json.containsKey("logseq")) {
                        MDC.put("logseq", json.get("logseq").toString());
                    }
                }
            }
        } catch (Exception e) {
        }
    }*/

}
