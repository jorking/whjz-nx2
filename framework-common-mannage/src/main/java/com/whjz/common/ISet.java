package com.whjz.common;

/**
 * (标记用的)
 * 实现此接口的类:
 * - 用途: 标志此类最后会转化为指定的类
 * - (注意: 实际上get方法上的@JSONField并不会触发)
 *
 * @param <T> 目标类,最终要转化为的类
 */
public interface ISet<T> {
    /**
     * 转换为相同字段的对象
     * @param targetCls 目标类
     * @return 目标对象
     */
    default T toEqual(Class<T> targetCls) {
        return ISetUtil.toEqual(this, targetCls);
    }

    /**
     * 合并入对象,相同字段不为null值进行覆盖(null值不会覆盖过去)
     * 会遍历对象内的成员变量,但不会对每个成员变量递归进行处理
     * @param targetCls 目标类
     * @param targetObj 目标对象
     */
    default void merge(Class<T> targetCls, T targetObj) {
        ISetUtil.merge(this, targetCls, targetObj);
    }

    /**
     * 合并入对象,相同字段不为null值进行覆盖(null值不会覆盖过去),
     * 需要目标对象对应字段值为null才覆盖
     * 会遍历对象内的成员变量,但不会对每个成员变量递归进行处理
     * @param targetCls 目标类
     * @param targetObj 目标对象
     */
    default void mergeNull(Class<T> targetCls, T targetObj) {
        ISetUtil.mergeNull(this, targetCls, targetObj);
    }
}
