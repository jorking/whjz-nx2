package com.whjz.common.converters;

import com.whjz.common.IConverter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * List为null时转空列表
 */
@Component
public class ListNullToEmptyConverter implements IConverter<List, List> {
    @Override
    public List convert(List from, String... args) {
        if (from == null) {
            return new ArrayList();
        }
        return from;
    }
}
