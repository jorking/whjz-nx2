package com.whjz.service;

import com.whjz.utils.CodeUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 抽象
 */
public interface Abstract {
    default Map<String, Object> success() {
        return success(new HashMap<>());
    }

    Map<String, Object> success(Object data);

    default Map<String, Object> fail(String code) {
        return fail(code, "失败");
    }

    default Map<String, Object> fail(String code, String msg) {
        return fail(code, msg, new HashMap<>());
    }


    default Map<String, Object> fail(CodeUtil codeUtil) {
        return fail(codeUtil.getCode(), codeUtil.getName());
    }

    /**
     * @param msg 失败原因
     */
    Map<String, Object> fail(String code, String msg, Object data);

    /**
     * 组装结果页面
     */
    Map page(List allList, int pageNum, int pageSize);

    /**
     * 组装结果页面
     */
    Map page(List pageList, int pageNum, int pageSize, int maxPage, int total);

    /**
     * 保存请求体里的客户号custNo
     */
    void saveCustNo(Map reqBody);

    /**
     * 获取保存的请求体里的客户号
     * @return 不存在返回null
     */
    String getCustNo();
}
