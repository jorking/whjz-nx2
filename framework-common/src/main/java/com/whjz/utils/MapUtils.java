package com.whjz.utils;

import java.util.HashMap;
import java.util.Map;

public class MapUtils {

    /**
     * @Author lcc
     * @Description 初始化map
     * @Date 15:18 2020/2/6
     * @Param
     * @return
    */
    public static Map InitMap(Object... args)   throws  Exception{
        Map map = new HashMap();
        if(args.length %2 != 0){
            throw  new Exception("参数必须为偶数个参数");
        }
        for(int i = 0; i < args.length ; i ++){
            map.put(args[i],args[i+1]);
            i = i+ 1;
        }
        return map;
    }
}
