package com.whjz.utils;


import java.util.HashMap;
import java.util.Map;

/**
 * 返回结果工具类
 */
public class ResultUtil {
	
    public static Map<String,Object>  resultMap(String msg,String code,Object data){
        return  resultMap(msg,code,data,null);
    }
    public static Map<String,Object>  resultMap(String msg,String code,Object data,String otherMsg){
        Map<String,Object> map = new HashMap<String,Object>();
        if(msg!=null){
            map.put("msg",msg);
        }else {
            map.put("msg","");
        }
        if(code!=null){
            map.put("code",code);
        }else{
            map.put("code","200");
        }
        /**
         * data部分包含ruleset、page、process、data
         */
        if(data!=null){
            map.put("data",data);

        }else {
            map.put("data","");
        }
        map.put("realMsg",otherMsg);
        return  map;
    }

}
