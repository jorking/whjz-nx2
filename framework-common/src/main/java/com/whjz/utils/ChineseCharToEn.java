package com.whjz.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 取得给定汉字串的首字母串,即声母串
 * Title: ChineseCharToEn
 * @date 
 * @author wangdajun
 */
public final class ChineseCharToEn {
	
 
	/**
	 * 字符串编码转换
	 * @param str 要转换编码的字符串
	 * @param charsetName 原来的编码
	 * @param toCharsetName 转换后的编码
	 * @return 经过编码转换后的字符串
	 */
	public static String conversionStr(String str, String charsetName,String toCharsetName) {
		try {
			str = new String(str.getBytes(charsetName), toCharsetName);
		} catch (UnsupportedEncodingException ex) {
			System.out.println("字符串编码转换异常：" + ex.getMessage());
		}
		return str;
	}
 ////xinzeng
	/**
	 * 将输入的汉字字符转换成拼音字符串返回， 如果是多音字则返回第一个读音，如果不是汉字则返回null
	 * 
	 * @param c
	 *            要转换的汉字字符
	 * @return
	 */
	public static String getPinyinString(char c) {
		String[] pinYinArray = PinyinHelper.toHanyuPinyinStringArray(c);
		if (pinYinArray == null || pinYinArray.length == 0)
			return null;
		else
			return pinYinArray[0];
	}

	private static Log logger = LogFactory
			.getLog(ChineseCharToEn.class);
/**
 * 带声调的返回
 * @param message
 * @return
 */
	public static String getPinyinString(String message) {
		if (message == null)
			return null;
		char[] chars = message.toCharArray();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			String[] pinYinArray = PinyinHelper
					.toHanyuPinyinStringArray(chars[i]);
			if (pinYinArray == null || pinYinArray.length == 0) {
				logger.warn("无法获取\"" + message + "\"中\"" + chars[i] + "\"的拼音！");
				return null;
			} else
				sb.append(pinYinArray[0]);
		}
		return sb.toString();
	}

	/**
	 * 将输入的汉字字符转换成拼音字符串返回，设置返回格式为小写不带音标， 如果是多音字则返回第一个读音，如果不是汉字则返回原文
	 * 
	 * @author limh
	 * @param c
	 *            要转换的汉字字符
	 * @return
	 */
	public static String getPinyinStringWithDefaultFormat(String message) {
		if (message == null || message.trim().length() == 0) {
			return "";
		}
		// 创建汉语拼音处理类
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		// 输出设置，大小写，音标方式
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		char[] chars = message.toCharArray();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			try {
				String[] pinYinArray = PinyinHelper.toHanyuPinyinStringArray(
						chars[i], defaultFormat);
				if (pinYinArray == null || pinYinArray.length == 0) {
					sb.append(Character.toLowerCase(chars[i]));
				} else {
					sb.append(pinYinArray[0]);
				}
			} catch (BadHanyuPinyinOutputFormatCombination e) {
				logger.warn("字符串转化为拼音出现异常：" + e);
			}
		}
		return sb.toString();
	}
	
	/**
	 * 中文转拼音之后 特殊处理后鼻音的问题
	 */
	public static String getPinyinStringWithSpecialFormat(String message) {
		if (message == null || message.trim().length() == 0) {
			return "";
		}
		// 创建汉语拼音处理类
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		// 输出设置，大小写，音标方式
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		char[] chars = message.toCharArray();
		String[] dataArray = new String[chars.length];
		String[] charsArray = new String[chars.length];
		String[] dealArray = new String[chars.length];
		List<String>  charList = new ArrayList<String>();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			try {
				String[] pinYinArray = PinyinHelper.toHanyuPinyinStringArray(
						chars[i], defaultFormat);
				if (pinYinArray == null || pinYinArray.length == 0) {
					sb.append(Character.toLowerCase(chars[i]));
					dataArray[i] = String.valueOf(Character.toLowerCase(chars[i]));
				} else {
					sb.append(pinYinArray[0]);
				}
			} catch (BadHanyuPinyinOutputFormatCombination e) {
				logger.warn("字符串转化为拼音出现异常：" + e);
			}
		}
		return sb.toString();
	}
	
	
	
	
	/**
	 * 获取指定资源的拼音首字母，如果资源不是中文字符串则返回null
	 * 
	 * @param messageKey
	 *            <bundle>:<key>
	 * @param locale
	 * @return
	 */


	public static Character getFirstPinyinChar(String message) {
		
		if (message == null || message.length() < 1)
			return null;
		String pinyin = getPinyinString(message.charAt(0));
		if (pinyin == null || pinyin.length() < 1)
			return null;
		char c = pinyin.charAt(0);
		c = Character.toUpperCase(c);
		return new Character(c);
	}	
	
	public static String getFirstLetter(String chinese) {
		Character character=getFirstPinyinChar(chinese);
		if(character == null) {
			return "#";
		}
		return character.toString();
	}
	
	
	public static float levenshtein(String str1,String str2) {
		//计算俩个字符串的长度
		int len1 = str1.length();
		int len2 = str2.length();
		//建立数组，比字符长度大一个空间
		int[][] dif = new int[len1+1][len2+1];
		//赋初值
		for(int a = 0;a<len1;a++) {
			dif[a][0] = a;
		}
		for(int a = 0;a<len2;a++) {
			dif[0][a] = a;
		}
		//计算俩个字符是否一样，计算左上的值
		int temp;
		for(int i=1;i<=len1;i++) {
			for(int j=1;j<=len2;j++) {
				if(str1.charAt(i-1) == str2.charAt(j-1)) {
					temp = 0;
				}else {
					temp = 1;
				}
				//取三个值中最小的
				dif[i][j] = min(dif[i-1][j-1]+temp,dif[i][j-1] + 1,dif[i-1][j]+1);
				//计算相似度
			}
		}
		float similarity = 1- (float) dif[len1][len2]/Math.max(str1.length(), str2.length());
		return similarity;
	}
	
	//得到最小值
    private static int min(int... is) {
    	int min = Integer.MAX_VALUE;
    	for(int i : is) {
    		if(min > i) {
    			min = i;
    		}
    	}
    	return min;
    }
	
	public static void main(String[] args) {
		
		String str1 = "huanjunming";
		String str2 = "huangjunmin";
		float levenshtein = levenshtein(str1,str2);
		//System.out.println("lingliangze".contains("tangmingwei")+" "+ "tangmingwei".contains("lingliangze"));
		/*ChineseCharToEn.getPinyinStringWithDefaultFormat("24小时");
		System.out.println("获取拼音首字母："+ ChineseCharToEn.getPinyinStringWithDefaultFormat("24小时"));
		*/
	}
	
}
	

