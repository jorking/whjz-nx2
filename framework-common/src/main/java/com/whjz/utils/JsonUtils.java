package com.whjz.utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>Title: JsonUtil</p>
 * <p>Description: JSON工具类</p>
 * @author xuzongtian
 * @date 2018年4月27日
 */
public class JsonUtils {

	/**
	 * <p>Title: JsonDateValueProcessor</p>
	 * <p>Description: 格式化日期</p>
	 * @author xuzongtian
	 * @date 2018年4月27日
	 */
	class JsonDateValueProcessor implements JsonValueProcessor {

		private String pattern = "yyyy-MM-dd HH:mm:ss";
		
		public JsonDateValueProcessor() {
			super();
		} 
		
		public JsonDateValueProcessor(String pattern) {
			super();
			this.pattern = pattern;
		}
		
		@Override
		public Object processArrayValue(Object value, JsonConfig config) {
			return process(value);
		}

		@Override
		public Object processObjectValue(String pattern, Object value, JsonConfig config) {
			return process(value);
		}
		
		private Object process(Object value) {
			if(value instanceof Date) {
				SimpleDateFormat dateFormat = new SimpleDateFormat(this.pattern);
				return dateFormat.format(value);
			}
			return null == value ? "" : value.toString();
		}
		
	}
	
	/**
	 * <p>Title: toJsonString</p>
	 * <p>Description: List转JSON字符串</p>
	 * @param objs
	 * @return
	 */
	public static <T> String toJsonString(List<T> objs) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonUtils().new JsonDateValueProcessor());
		JSONArray jsonArray = JSONArray.fromObject(objs, jsonConfig);
		return jsonArray.toString();
	}
	
	/**
	 * <p>Title: toJsonString</p>
	 * <p>Description: javaBean转JSON字符串</p>
	 * @param obj
	 * @return
	 */
	public static <T> String toJsonString(T obj) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonUtils().new JsonDateValueProcessor());
		JSONObject jsonObject = JSONObject.fromObject(obj, jsonConfig);
		return jsonObject.toString();
	}
	
	
	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSON字符串转List</p>
	 * @param jsonString
	 * @param classBean
	 * @param classMap
	 * @return
	 */
	public static <T> List<T> toList(String jsonString, Class classBean, Map<String, Class> classMap) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		List<T> itemsT = JSONArray.toList(jsonArray, classBean, classMap);
		return itemsT;
	}
	
	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSON字符串转List</p>
	 * @param jsonString
	 * @param classBean
	 * @return
	 */
	public static <T> List<T> toList(String jsonString, Class classBean) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		List<T> itemsT = JSONArray.toList(jsonArray, classBean);
		return itemsT;
	}
	
	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSON字符串转List</p>
	 * @param jsonString
	 * @return
	 */
	public static <T> List<T> toList(String jsonString) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		List<T> itemsT = JSONArray.toList(jsonArray);
		return itemsT;
	}
	
	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSONArray转List</p>
	 * @param jsonArray
	 * @param classBean
	 * @param map
	 * @return
	 */
	public static <T> List<T> toList(JSONArray jsonArray, Class<T> classBean, Map<String, Class> map) {
		List<T> itemsT = JSONArray.toList(jsonArray, classBean, map);
		return itemsT;
	}
	
	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSONArray转List</p>
	 * @param jsonArray
	 * @param classBean
	 * @return
	 */
	public static <T> List<T> toList(JSONArray jsonArray, Class<T> classBean) {
		List<T> itemsT = JSONArray.toList(jsonArray, classBean);
		return itemsT;
	}

	/**
	 * <p>Title: toList</p>
	 * <p>Description: JSONArray转List</p>
	 * @param jsonArray
	 * @return
	 */
	public static <T> List<T> toList(JSONArray jsonArray) {
		List<T> itemsT = JSONArray.toList(jsonArray);
		return itemsT;
	}
	
}
