package com.whjz.utils.mask;

public interface IFieldMaskable
{

    boolean getMaskFlag();
    int getFieldLen();
    int getMaskBeginIndex();
    int getMaskEndIndex();
    String[] getMaskFields();

    default int getMaskLen()
    {
        if(getMaskBeginIndex() <= 0 || getMaskEndIndex() <= 0)
        {
            return -1;
        }

        if(getMaskBeginIndex() > getFieldLen() || getMaskEndIndex() > getFieldLen())
        {
            return  -1;
        }

        if(getMaskBeginIndex() > getMaskEndIndex())
        {
            return -1;
        }

        return getMaskEndIndex() - getMaskBeginIndex() + 1;
    }

    default void setFieldLen(int length)
    {
        throw new UnsupportedOperationException();
    }
}
