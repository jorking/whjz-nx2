package com.whjz.utils.mask;

import java.util.Arrays;

public class VariableLenFieldMaskImpl implements IFieldMaskable
{
    // true: 掩码；  false : 不掩码；
    private final boolean ADDRESS_MASK_FLAG = true;

    // 掩码因子， 不掩码 0 , 全部掩码 1,  取值在 0.01 ~ 1.00, 取值越接近 1 被淹盖内容越多
    private float factor = 0.40F;

    // 报文中所有需要掩码的变长字段字段名，按照逗号分隔， 全部添加在这个数组里
    private final String[] ADDRESS_FIELD = {"address", "dtlAddr", "identRgstAdr", "CstNm", "cstNm", "cstAcctNm", "userName", "BeChkPrsnNm", "corprtnNm"};


    /**********  begin 单例模式 返回对象 ****************************/
    private static VariableLenFieldMaskImpl instance = new VariableLenFieldMaskImpl();
    private VariableLenFieldMaskImpl(){};
    public static VariableLenFieldMaskImpl getInstance()
    {
        return instance;
    }
    /**********  end 单例模式 返回对象 ****************************/


    private final float diff = 0.001F;
    /**************************************************************/
    // 被淹码字段长度, 由于是可变长度，需要在具体得到被淹码字符串之后设置到这个字段中
    private int ADDRESS_LEN = 0;
    @Override
    public void setFieldLen(int length)
    {
        ADDRESS_LEN = length;
    }
    /**************************************************************/

    @Override
    public boolean getMaskFlag()
    {
        // 掩码因子为 0 不掩码
        if (Math.abs(factor) < diff)
        {
            System.out.println("factor 设置为0 不掩码， factor=[" + factor + "]");
            return false;
        }

        // 掩码因子值大于 1 ，值设置有误，不掩码
        // 掩码因子为负值，   值设置有误， 不掩码
        if (factor > 1  || factor < 0)
        {
            System.out.println("factor 设置有误， factor=[" + factor + "]");
            return false;
        }

        return ADDRESS_MASK_FLAG;
    }

    @Override
    public int getFieldLen()
    {
        return ADDRESS_LEN;
    }

    @Override
    public int getMaskBeginIndex()
    {
        // 掩码因子为 1 全掩码, 索引值从1开始
        if (Math.abs(1-factor) < diff)
        {
            return 1;
        }

        int maskBeginIndex = getMiddleIndex() - getMaskMiddleIndex();
        return maskBeginIndex;
    }


    @Override
    public int getMaskEndIndex()
    {
        // 掩码因子为 1 全掩码
        if (Math.abs(1-factor) < diff)
        {
            return getFieldLen();
        }

        int maskEndIndex = getMiddleIndex() + getMaskMiddleIndex();
        return maskEndIndex;
    }

    @Override
    public String[] getMaskFields()
    {
        return ADDRESS_FIELD;
    }

    @Override
    public String toString()
    {
        return "\nADDRESS_MASK_FLAG=["+getMaskFlag()+ "],\n"
                + "getFieldLen()=[" + getFieldLen()  + "],\n"
                + "factor=[" +  factor + "],\n"
                + "getMaskBeginIndex=[" +  getMaskBeginIndex() + "],\n"
                + "getMaskEndIndex=[" +  getMaskEndIndex() + "],\n"
                + "getMaskFields()=[" + Arrays.asList(getMaskFields()).toString() + "]\n";
    }


    private int getMaskMiddleIndex()
    {
        // 向下取整， 0.1->0, 0.9 ->0, 1.9->1, 2.1 ->2, 2.5->2, 2.9->2
        float maskLen = factor * getFieldLen();
        int maskMiddleIndex = (int)Math.floor(maskLen /2);
        return maskMiddleIndex;
    }

    private int getMiddleIndex()
    {
        // 向上取整， 0.1->1, 0.9->1, 1.9->2, 2.1->3, 2.5->3, 2.9->3
        // 此处 getFieldLen() 返回时 int  需要强转为 float， 否则除 2 时按照整型计算，返回的是截断后的整型
        int middleIndex = (int)Math.ceil((float)getFieldLen()/2);
        return middleIndex;
    }

}
