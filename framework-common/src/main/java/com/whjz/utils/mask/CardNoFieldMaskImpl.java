package com.whjz.utils.mask;

import java.util.Arrays;

public class CardNoFieldMaskImpl implements IFieldMaskable
{
    // true: 掩码；  false : 不掩码；
    private final boolean CARD_NO_MASK_FLAG = true;

    // 被淹码字段长度, 卡号是 19 位长
    private final int CARD_NO_LEN = 19;



    // 被淹码字段第一位是 1，
    // eg.  1234567890ABCDEFGHI
    //      CARD_NO_MASK_BEG_INDEX = 2
    //      CARD_NO_MASK_END_INDEX = 18
    //      表示 从第二位 至 第 18 位掩码
    //      掩码结果为： 1******************I
    private final int CARD_NO_MASK_BEG_INDEX = 9;
    private final int CARD_NO_MASK_END_INDEX = 13;

    // 报文中所有表示身份证的字段名，按照逗号分隔， 全部添加在这个数组里
    private final String[] CARD_NO_FIELD = {"OldAcctNo", "newAcctNo", "CstAcctNo", "AcctNo"};




    /**********  begin 单例 模式 返回对象 ****************************/
    private static CardNoFieldMaskImpl instance = new CardNoFieldMaskImpl();
    private CardNoFieldMaskImpl(){};
    public static CardNoFieldMaskImpl getInstance()
    {
        return instance;
    }
    /**********  end 单例 模式 返回对象 ****************************/

    @Override
    public boolean getMaskFlag()
    {
        return CARD_NO_MASK_FLAG;
    }

    @Override
    public int getFieldLen()
    {
        return CARD_NO_LEN;
    }

    @Override
    public int getMaskBeginIndex()
    {
        return CARD_NO_MASK_BEG_INDEX;
    }

    @Override
    public int getMaskEndIndex()
    {
        return  CARD_NO_MASK_END_INDEX;
    }

    @Override
    public String[] getMaskFields()
    {
        return CARD_NO_FIELD;
    }

    @Override
    public String toString()
    {
        return "\ngetMaskFlag=["+ getMaskFlag() + "],\n"
                + "getFieldLen=[" + getFieldLen()+ "],\n"
                + "getMaskBeginIndex=[" + getMaskBeginIndex()  + "],\n"
                + "getMaskEndIndex=[" +  getMaskEndIndex() + "],\n"
                + "getMaskFields=[" + Arrays.asList(getMaskFields()).toString() + "]\n";
    }
}
