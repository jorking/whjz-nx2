package com.whjz.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Title: String2Utils</p>
 * <p>Description: 字符串正则校验工具</p>
 * @author gzx
 * @date 2019年11月07日
 */
public class StringRegUtils {

    /**手机号码正则匹配*/
    public static final String REGEX_PHONE="^1[1-9][0-9]\\d{4,8}$";


    /**
     * 取消字符中空格
     * @return String
     */
    public static String getStringRemoveBlank(String string){
       if (StringUtils.isNotBlank(string)){
           string=string.replaceAll(" ","");
       }else {
           return null;
       }
        return string;
    }


    /**
     * 验证是否是手机号码
     * @return String
     */
    public static Boolean isPhone(String phone){
        Boolean isPhone=false;
        phone=getStringRemoveBlank(phone);
        if (StringUtils.isNotBlank(phone)){
            isPhone=phone.matches(REGEX_PHONE);
        }
        return isPhone;
    }

 
}
