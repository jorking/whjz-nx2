package com.whjz.utils;

/**
 * <p>Title: Cache</p>
 * <p>Description: 缓存</p>
 * @author xuzongtian
 * @date 2018年4月22日
 */
public class Cache {

	// 缓存数据
	private Object value;

	/**
	 * <p>Title: getValue</p>
	 * <p>Description: 缓存数据</p>
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * <p>Title: setValue</p>
	 * <p>Description: 缓存数据</p>
	 * @param value
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
}
