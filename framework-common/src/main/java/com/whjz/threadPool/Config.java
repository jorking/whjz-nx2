package com.whjz.threadPool;

public class Config {
    /************ 线程池配置 *******************/
    // 线程池核心线程数
    private static int corePoolSize;
    // 线程池最大线程数
    private static int maximumPoolSize;
    // 线程池队列长度（线程池达到最大线程数后放入队列等待）
    private static int threadPoolQueueSize;
    // 线程池线程存活时间，超时后直接杀死未完成的线程
    private static long keepAliveTime;

    // 线程池核心线程数
    private static final int defaultCorePoolSize = Runtime.getRuntime().availableProcessors()*3;
    // 线程池最大线程数
    private static final int defaultMaximumPoolSize = 50;
    // 线程池队列长度（线程池达到最大线程数后放入队列等待）
    private static final int defaultThreadPoolQueueSize = 500;
    // 线程池线程存活时间，超时后直接杀死未完成的线程,单位秒
    private static final long defaultKeepAliveTime = 7 * 24 * 3600;

    static {
        loadConfig();
    }

    private static void loadConfig() {

        corePoolSize = SystemConfigUtil.getInteger("corePoolSize",
                defaultCorePoolSize);
        maximumPoolSize = SystemConfigUtil.getInteger("maximumPoolSize",
                defaultMaximumPoolSize);
        threadPoolQueueSize = SystemConfigUtil.getInteger("threadQueueSize",
                defaultThreadPoolQueueSize);
        keepAliveTime = SystemConfigUtil.getLong("keepAliveTime",
                defaultKeepAliveTime);
    }

    public static int getCorePoolSize() {
        return corePoolSize;
    }

    public static void setCorePoolSize(int corePoolSize) {
        Config.corePoolSize = corePoolSize;
    }

    public static int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public static void setMaximumPoolSize(int maximumPoolSize) {
        Config.maximumPoolSize = maximumPoolSize;
    }

    public static int getThreadPoolQueueSize() {
        return threadPoolQueueSize;
    }

    public static void setThreadPoolQueueSize(int threadPoolQueueSize) {
        Config.threadPoolQueueSize = threadPoolQueueSize;
    }

    public static long getKeepAliveTime() {
        return keepAliveTime;
    }

    public static void setKeepAliveTime(long keepAliveTime) {
        Config.keepAliveTime = keepAliveTime;
    }
}