package com.whjz.threadPool;

public class ThreadPoolFactory {
	private static ThreadPool pool;
	private static ThreadPoolConfig threadPoolConfig;

	private static void init() {
		threadPoolConfig = new ThreadPoolConfig(Config.getCorePoolSize(),
				Config.getMaximumPoolSize(), Config.getKeepAliveTime(),
				Config.getThreadPoolQueueSize());
		pool = new ThreadPool(threadPoolConfig);
	}

	public static ThreadPool getThreadPool() {
		synchronized (ThreadPoolFactory.class) {
			if (pool == null) {
				init();
			}
			return pool;
		}
	}
}