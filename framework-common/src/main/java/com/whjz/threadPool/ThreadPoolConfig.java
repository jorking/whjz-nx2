package com.whjz.threadPool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ThreadPoolConfig {
    private int corePoolSize;
    private int maximumPoolSize;
    private long keepAliveTime;
    private int threadPoolQueueSize;
    private TimeUnit unit = TimeUnit.HOURS;
    private ArrayBlockingQueue<Runnable> workQueue;

    public ThreadPoolConfig(int corePoolSize, int maximumPoolSize,
                            long keepAliveTime, int threadPoolQueueSize) {
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.keepAliveTime = keepAliveTime;
        this.threadPoolQueueSize = threadPoolQueueSize;
        workQueue = new ArrayBlockingQueue<Runnable>(threadPoolQueueSize);
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    public int getThreadPoolQueueSize() {
        return threadPoolQueueSize;
    }

    public TimeUnit getUnit() {
        return unit;
    }

    public ArrayBlockingQueue<Runnable> getWorkQueue() {
        return workQueue;
    }

}
