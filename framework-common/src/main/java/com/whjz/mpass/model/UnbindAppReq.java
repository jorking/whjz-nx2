package com.whjz.mpass.model;

import lombok.Data;

/**
 * 解绑请求
 * @author youkun
 * @data 2.18/7/6
 */
@Data
public class UnbindAppReq extends BaseParam{

    /**
     * 目标设备token
     */
    private String deliveryToken;

    /**
     * 要解绑的用户标识
     */
    private String userId;
}
