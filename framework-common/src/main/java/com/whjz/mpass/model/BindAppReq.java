package com.whjz.mpass.model;

import lombok.Data;

/**
 * 手机绑定请求
 * @author youkun
 * @data 2018/7/6
 */
@Data
public class BindAppReq extends BaseParam {

    /**
     * 目标设备token
     */
    private String deliveryToken;

    /**
     * 设备类型
     * 1为android
     * 2为ios
     */
    private int osType;

    /**
     * 绑定的用户标识
     */
    private String userId;

}
