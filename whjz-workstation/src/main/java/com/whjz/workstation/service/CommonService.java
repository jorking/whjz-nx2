package com.whjz.workstation.service;

import java.util.Map;

public interface CommonService {

    /**
     * 发送短信
     *
     * @param req
     * @return
     */
    Map<String, Object> sendNote(Map<String, String> req);

    /**
     * 校验短信
     *
     * @param req
     * @return
     */
    Map<String, Object> checkNote(Map<String, String> req);
}
