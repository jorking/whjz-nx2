package com.whjz.workstation.service.impl;

import com.whjz.entity.MbBackUser;
import com.whjz.service.impl.AbstractServiceImpl;
import com.whjz.utils.StringUtils;
import com.whjz.workstation.dao.ManageUserMapper;
import com.whjz.workstation.service.ManageUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 组织机构查询事务类
 */
@Slf4j
@Service
public class ManageUserServiceImpl  extends AbstractServiceImpl implements ManageUserService {

    @Autowired
    @SuppressWarnings("all")
    ManageUserMapper manageUserMapper;

    /**
     * @Desc :查询本地数据库柜员信息
     * @Author :tlf    @Date:2020/9/10 17:10
     * @Param:      @Return:
     */
    @Override
    public MbBackUser queryBankUserByUserId(Map<String, String> req) {
        MbBackUser mbBackUser =new MbBackUser();
        try {
            String userId = StringUtils.getString(req.get("userId"));
            mbBackUser = manageUserMapper.selectByUserId(userId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return mbBackUser;
    }
}
