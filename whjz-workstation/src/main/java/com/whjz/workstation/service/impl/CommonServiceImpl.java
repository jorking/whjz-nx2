package com.whjz.workstation.service.impl;

import com.whjz.redis.RedisUtils;
import com.whjz.service.impl.AbstractServiceImpl;
import com.whjz.utils.*;
import com.whjz.workstation.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 公共方法类
 */
@Slf4j
@Service
public class CommonServiceImpl extends AbstractServiceImpl implements CommonService {


    @Autowired
    @SuppressWarnings("all")
    private Environment environment;

    /**
     * //短信验证
     */
    private static final String SYMBOLS = "0123456789"; // 数字
    private static final Random RANDOM = new SecureRandom();

    /**
     * 发送短信 规则如下：
     * 1.将产生一个uuid的唯一标识和一个随机码
     * 2.在redis集群中 key为唯一标识 value为随机码
     * 3.根据上送的noteTemplateType来确定启用什么模板
     * 4.通过esb接口发送短信
     *
     * @param req
     * @return
     */
    @Override
    public Map<String, Object> sendNote(Map<String, String> req){
        try {
            //1.将产生一个uuid的唯一标识和一个随机码
            String uuid = String2Utils.getGenerateUUID();
            //String randNo = getNonce_str();
            String randNo = "251314";
            String custNo = StringUtils.getString(req.get("custNo"));
            String noteContext = "亲爱的，你的验证码是" + randNo;
            Long lostTime = environment.getProperty("redis.noteValidTimeInSeconds", Long.class);
            //验证码发送方法
            Map resMap = new HashMap();
            resMap.put("uuid", uuid);
            resMap.put("noteNo", noteContext);
            String uuidUserId = uuid + custNo;
            RedisUtils.setKeyAndCacheTime(uuidUserId, randNo, lostTime);
            return ResultUtil.resultMap("发送成功", ResultCode.RESULT_CODE_SUCCESS.code(), resMap);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResultUtil.resultMap("发送失败", ResultCode.RESUTL_CODE_FAILURE.code(), null);
    }

    /**
     * 校验短信 规则如下
     * 1.根据唯一标识确认redis集群中是否包含 未包含则返回 已失效
     * 2.包含校验随机码
     *
     * @param req
     * @return
     */
    @Override
    public Map<String, Object> checkNote(Map<String, String> req) {
        try {
            String uuid = req.get("uuid");
            String randNo = req.get("noteNo");
            String custNo = StringUtils.getString(req.get("custNo"));
            String uuidUserId = uuid + custNo;
            String redisRandNo = RedisUtils.getValue(uuidUserId);
            if (StringUtils.isNotNullOrEmtory(redisRandNo)) {
                if (redisRandNo.equals(randNo)) {
                    return ResultUtil.resultMap("校验成功", ResultCode.RESULT_CODE_SUCCESS.code(), new HashMap<>());
                } else {
                    return ResultUtil.resultMap("验证码输入错误", ResultCode.RESUTL_CODE_FAILURE.code(), new HashMap<>());
                }
            } else {
                return ResultUtil.resultMap("验证码已失效,请重新发送短信", ResultCode.RESUTL_CODE_FAILURE.code(), new HashMap<>());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResultUtil.resultMap("验证码校验异常，请重试", ResultCode.RESUTL_CODE_FAILURE.code(), null);
    }



    /**
     * 获取长度为 6 的随机数字 用来做短信验证吗
     *
     * @return 随机数字
     */
    public static String getNonce_str() {

        // 如果需要4位，那 new char[4] 即可，其他位数同理可得
        char[] nonceChars = new char[6];

        for (int index = 0; index < nonceChars.length; ++index) {
            nonceChars[index] = SYMBOLS.charAt(RANDOM.nextInt(SYMBOLS.length()));
        }

        return new String(nonceChars);
    }

}
