package com.whjz.workstation.aop;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Slf4j
public class CoreFilter implements Filter{


    public CoreFilter(){

    }
    public void init(FilterConfig filterConfig) {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        String[] params = {"custNo","userId","logseq"};

        Map<String,Object> map = new HashMap<>();
        for (String param : params) {
            String parm = request.getHeader(param);
            if(Objects.nonNull(parm)){
                map.put(param,parm);
            }
        }

        //执行业务逻辑
        if(map.get("custNo") != null ) {
            MDC.put("custNo", map.get("custNo").toString());
        }
        if(map.get("userId") != null ) {
            MDC.put("custNo", map.get("userId").toString());
        }
        if(map.get("logseq") != null ) {
            MDC.put("logseq", map.get("logseq").toString());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }


}
