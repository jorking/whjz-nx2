package com.whjz.workstation.dao;

import com.whjz.entity.MbBackUser;

public interface ManageUserMapper {

    MbBackUser selectByUserId(String userId); // 根据用户号查找柜员信息**/

}
