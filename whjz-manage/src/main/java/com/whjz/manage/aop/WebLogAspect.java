package com.whjz.manage.aop;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @Description todo
 * @Author lcc
 * @Date 2019/4/29 11:11
 **/
//@Aspect
@Component
public class WebLogAspect {

    @Pointcut("execution(public * com.whjz.manage.controller.*.*(..))")
    public void webLog(){

    }
    /*//@Before("webLog()")
    public void before(JoinPoint joinPoint) throws  Throwable{
        try {
            Object[] args =  joinPoint.getArgs();
            if(args != null && args.length > 0 ){
                Object obj = args[0];
                if(obj instanceof  Map) {
                    Map map = (Map)obj;
                    if(map.get("custNo") != null ) {
                        MDC.put("custNo", map.get("custNo").toString());
                    }
                    if(map.get("userId") != null ) {
                        MDC.put("custNo", map.get("userId").toString());
                    }
                    if(map.get("logseq") != null ) {
                        MDC.put("logseq", map.get("logseq").toString());
                    }
                }else if(obj instanceof String) {
                    JSONObject json = JSONObject.parseObject(obj.toString());
                    if (json.containsKey("custNo")) {
                        MDC.put("custNo", json.get("custNo").toString());
                    }
                    if (json.containsKey("userId")) {
                        MDC.put("custNo", json.get("userId").toString());
                    }
                    if (json.containsKey("logseq")) {
                        MDC.put("logseq", json.get("logseq").toString());
                    }
                }
            }
        } catch (Exception e) {
        }
    }*/

    /**
     *
     * @param
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        CoreFilter httpBasicFilter = new CoreFilter();
        registrationBean.setFilter(httpBasicFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        return registrationBean;
    }

}
