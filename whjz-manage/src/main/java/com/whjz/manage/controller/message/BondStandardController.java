package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.BondStandardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-6
 * remarks:供应商保证金类
 */
@Api(tags = "供应商保证金标准")
@Slf4j
@RestController
@RequestMapping("BondStandard")
public class BondStandardController {
    @Autowired
    @SuppressWarnings("all")
    private BondStandardService bondStandardService;

    @PostMapping("/queryBondStandardList")
    @ApiOperation(value = "查询供应商保证金标准列表", notes = "根据条件供应商保证金标准返回供应商保证金标准列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondPrice", value = "保证金金额", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondGroud", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "remark", value = "备注", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryBondStandardList(@RequestBody Map<String, String> paramMap) {

        return bondStandardService.queryBondStandardList(paramMap);
    }

    @PostMapping("/insertBondStandard")
    @ApiOperation(value = "添加供应商保证金标准列表", notes = "添加供应商保证金标准列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondPrice", value = "保证金金额", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondGroud", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "remark", value = "备注", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object>insertBondStandard(@RequestBody Map<String, String> ParamMap) {
        return bondStandardService.insertBondStandard(ParamMap);
    }
    @PostMapping("/updateBondStandard")
    @ApiOperation(value = "修改供应商保证金标准列表", notes = "修改供应商保证金列表标准")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondPrice", value = "保证金金额", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondGroud", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "remark", value = "备注", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
})
public Map<String, Object> updateBondStandard(@RequestBody Map<String, String> ParamMap) {
        return bondStandardService.updateBondStandard(ParamMap);
    }
    @PostMapping("/deleteBondStandard")
    @ApiOperation(value = "删除供应商保证金标准列表", notes = "删除供应商保证金标准列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteBondStandard(@RequestBody Map<String,String> paramMap) {
        return bondStandardService.deleteBondStandard(paramMap);
    }
}