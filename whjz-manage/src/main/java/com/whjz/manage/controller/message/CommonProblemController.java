package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.CommonProblemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-11
 * remarks:常见问题类
 */
@Api(tags = "常见问题类")
@Slf4j
@RestController
@RequestMapping("commonProblem")
public class CommonProblemController {
    @Autowired
    @SuppressWarnings("all")
    private CommonProblemService commonProblemService;

    @PostMapping("/queryCommonProblemList")
    @ApiOperation(value = "查询常见问题列表", notes = "根据条件查询常见问题返回常见问题列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "内容", paramType = "query", required = true),
            @ApiImplicitParam(name = "keywords", value = "关键字", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryCommonProblemList(@RequestBody Map<String, String> paramMap) {

        return commonProblemService.queryCommonProblemList(paramMap);
    }

    @PostMapping("/insertCommonProblem")
    @ApiOperation(value = "添加常见问题列表", notes = "添加常见问题列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "内容", paramType = "query", required = true),
            @ApiImplicitParam(name = "keywords", value = "关键字", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertCommonProblem(@RequestBody Map<String, String> ParamMap) {
        return commonProblemService.insertCommonProblem(ParamMap);
    }

    @PostMapping("/updateCommonProblem")
    @ApiOperation(value = "修改常见问题列表", notes = "修改常见问题列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "内容", paramType = "query", required = true),
            @ApiImplicitParam(name = "keywords", value = "关键字", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateCommonProblem(@RequestBody Map<String, String> ParamMap) {
        return commonProblemService.updateCommonProblem(ParamMap);
    }

    @PostMapping("/deleteCommonProblem")
    @ApiOperation(value = "删除常见问题列表", notes = "删除常见问题列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteFeedback(@RequestBody Map<String, String> paramMap) {
        return commonProblemService.deleteCommonProblem(paramMap);
    }
}

