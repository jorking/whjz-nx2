package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.EvaluateService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：评价相关接口
 */
@Api(tags = {"评价管理"})
@RestController
@Slf4j
@RequestMapping("evaluate")
public class EvaluateController extends AbstractControllerImpl {

    @Autowired
    private EvaluateService evaluateService;

    @PostMapping("/queryEvaluateList")
    @ApiOperation(value = "查询评价列表", notes = "根据查询条件返回评价列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryEvaluateList(@RequestBody Map<String, String> paramMap) {
        return evaluateService.queryEvaluateList(paramMap);
    }

    @PostMapping("/insertEvaluate")
    @ApiOperation(value = "添加评价", notes = "添加评价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productCode", value = "商品编号", paramType = "query"),
            @ApiImplicitParam(name = "frontSeqNo", value = "前端流水号", paramType = "query", required = true),
            @ApiImplicitParam(name = "backSeqNo", value = "后端流水号", paramType = "query"),
            @ApiImplicitParam(name = "content", value = "评价内容", paramType = "query"),
            @ApiImplicitParam(name = "evaluateDate", value = "评价时间", paramType = "query"),
            @ApiImplicitParam(name = "platformRatingStar", value = "平台评价星级", paramType = "query"),
            @ApiImplicitParam(name = "productRatingStar", value = "产品评价星级", paramType = "query", required = true),
            @ApiImplicitParam(name = "comLeaderRatingStar", value = "社区团长评价星级", paramType = "query", required = true),
            @ApiImplicitParam(name = "evaluateUser", value = "评价人", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertEvaluate(@RequestBody Map<String, String> paramMap) {
        return evaluateService.insertEvaluate(paramMap);
    }

    @PostMapping("/updateEvaluate")
    @ApiOperation(value = "修改评价", notes = "修改评价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateCode", value = "评价编号", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateName", value = "评价名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "EvaluateType", value = "评价类型1-供应商评价 2-物流评价 3-驿站评价", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateFirstParty", value = "评价甲方", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateSecondParty", value = "评价乙方", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateDesc", value = "评价概况", paramType = "query"),
            @ApiImplicitParam(name = "dueDate", value = "评价到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "signDate", value = "评价签订日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "EvaluateState", value = "评价期限1、正常2、评价已超期", paramType = "query", required = true),
            @ApiImplicitParam(name = "signPlace", value = "签订地点", paramType = "query"),
            @ApiImplicitParam(name = "EvaluateFile", value = "评价文件", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "评价录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "评价创建时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateEvaluate(@RequestBody Map<String, String> paramMap) {
        return evaluateService.updateEvaluate(paramMap);
    }

    @PostMapping("/deleteEvaluate")
    @ApiOperation(value = "删除评价", notes = "删除评价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteEvaluate(@RequestBody Map<String, String> paramMap) {
        return evaluateService.deleteEvaluate(paramMap);
    }
}
