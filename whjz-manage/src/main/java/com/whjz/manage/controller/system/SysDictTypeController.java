package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysDictTypeService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典类别相关接口
 */

@Api(tags = {"字典类别管理"})
@RestController
@Slf4j
@RequestMapping("sysDictType")
public class SysDictTypeController extends AbstractControllerImpl {

    @Autowired
    private SysDictTypeService sysDictTypeService;

    @PostMapping("/queryDictTypeList")
    @ApiOperation(value = "查询字典类别列表", notes = "根据查询条件返回字典类别列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictTypeCode",value = "字典类型编码  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "dictTypeName",value = "字典类型名称  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryDictTypeList(@RequestBody Map<String,String> paramMap) {
        return sysDictTypeService.queryDictTypeList(paramMap);
    }

    @PostMapping("/insertDictType")
    @ApiOperation(value = "添加字典类别", notes = "添加字典类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictTypeId",value = "字典类别ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeCode",value = "字典类型编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeName",value = "字典类型名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query"),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertDictType(@RequestBody Map<String,String> paramMap) {
        return sysDictTypeService.insertDictType(paramMap);
    }

    @PostMapping("/updateDictType")
    @ApiOperation(value = "修改字典类别", notes = "修改字典类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictTypeId",value = "字典类别ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeCode",value = "字典类型编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeName",value = "字典类型名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query"),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateDictType(@RequestBody Map<String,String> paramMap) {
        return sysDictTypeService.updateDictType(paramMap);
    }

    @PostMapping("/deleteDictType")
    @ApiOperation(value = "删除字典类别", notes = "删除字典类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteDictType(@RequestBody Map<String,String> paramMap) {
        return sysDictTypeService.deleteDictType(paramMap);
    }
}
