package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.ProductService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-1
 * remarks：产品相关接口
 */
@Api(tags = {"产品管理"})
@RestController
@Slf4j
@RequestMapping("product")
public class ProductController extends AbstractControllerImpl {

    @Autowired
    private ProductService productService;

    @PostMapping("/queryProductList")
    @ApiOperation(value = "查询产品列表", notes = "根据查询条件返回产品列表")
    @ApiImplicitParams({
           //待填
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryProductList(@RequestBody Map<String, String> paramMap) {
        return productService.queryProductList(paramMap);
    }

    @PostMapping("/insertProduct")
    @ApiOperation(value = "添加产品", notes = "添加产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productCode", value = "产品编号", paramType = "query"),
            @ApiImplicitParam(name = "productName", value = "产品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productType", value = "产品分类 三级联动", paramType = "query"),
            @ApiImplicitParam(name = "productBusinessId", value = "商家id", paramType = "query"),
            @ApiImplicitParam(name = "productBusinessName", value = "商家名称", paramType = "query"),
            @ApiImplicitParam(name = "productSpecifications", value = "规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query", required = true),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query", required = true),
            @ApiImplicitParam(name = "crazyRequisitionDate", value = "疯狂请购时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "salesVolume", value = "销售量", paramType = "query"),
            @ApiImplicitParam(name = "productDetails", value = "商品详情", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "录入时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertProduct(@RequestBody Map<String, String> paramMap) {
        return productService.insertProduct(paramMap);
    }

    @PostMapping("/updateProduct")
    @ApiOperation(value = "修改产品", notes = "修改产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "productCode", value = "产品编号", paramType = "query"),
            @ApiImplicitParam(name = "productName", value = "产品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productType", value = "产品分类 三级联动", paramType = "query"),
            @ApiImplicitParam(name = "productBusinessId", value = "商家id", paramType = "query"),
            @ApiImplicitParam(name = "productBusinessName", value = "商家名称", paramType = "query"),
            @ApiImplicitParam(name = "productSpecifications", value = "规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query", required = true),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query", required = true),
            @ApiImplicitParam(name = "crazyRequisitionDate", value = "疯狂请购时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "salesVolume", value = "销售量", paramType = "query"),
            @ApiImplicitParam(name = "productDetails", value = "商品详情", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "录入时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateProduct(@RequestBody Map<String, String> paramMap) {
        return productService.updateProduct(paramMap);
    }

    @PostMapping("/deleteProduct")
    @ApiOperation(value = "删除产品", notes = "删除产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteProduct(@RequestBody Map<String, String> paramMap) {
        return productService.deleteProduct(paramMap);
    }
}
