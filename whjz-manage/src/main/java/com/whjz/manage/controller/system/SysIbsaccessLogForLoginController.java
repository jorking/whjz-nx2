package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysIbsaccessLogForLoginService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：登录日志相关接口
 */

@Api(tags = {"登录日志管理"})
@RestController
@Slf4j
@RequestMapping("sysIbsaccessLogForLogin")
public class SysIbsaccessLogForLoginController extends AbstractControllerImpl {

    @Autowired
    private SysIbsaccessLogForLoginService sysIbsaccessLogForLoginService;

    @PostMapping("/queryIbsaccessLogForLoginList")
    @ApiOperation(value = "查询登录日志列表", notes = "根据查询条件返回登录日志列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "deptId",value = "机构编号",paramType = "query"),
            @ApiImplicitParam(name = "address",value = "地址",paramType = "query"),
            @ApiImplicitParam(name = "deviceId",value = "设备编号",paramType = "query"),
            @ApiImplicitParam(name = "deviceType",value = "设备类型",paramType = "query"),
            @ApiImplicitParam(name = "loginState",value = "登录状态",paramType = "query"),
            @ApiImplicitParam(name = "startDate",value = "开始时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "endDate",value = "结束时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "phoneModel",value = "手机型号",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryIbsaccessLogForLoginList(@RequestBody Map<String,String> paramMap) {
        return sysIbsaccessLogForLoginService.queryIbsaccessLogForLoginList(paramMap);
    }

    @PostMapping("/insertIbsaccessLogForLogin")
    @ApiOperation(value = "添加登录日志", notes = "添加登录日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptId",value = "用户机构编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "address",value = "地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "bankType",value = "银行类型",paramType = "query"),
            @ApiImplicitParam(name = "clientNo",value = "客户编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deviceId",value = "设备编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deviceType",value = "设备类型",paramType = "query"),
            @ApiImplicitParam(name = "loginState",value = "登录状态 1-成功 2-失败 3-首次登陆",paramType = "query", required = true),
            @ApiImplicitParam(name = "latitude",value = "纬度",paramType = "query", required = true),
            @ApiImplicitParam(name = "longitude",value = "经度",paramType = "query", required = true),
            @ApiImplicitParam(name = "loginDate",value = "登录时间",paramType = "query"),
            @ApiImplicitParam(name = "logoutDate",value = "登出时间",paramType = "query"),
            @ApiImplicitParam(name = "network",value = "网络",paramType = "query"),
            @ApiImplicitParam(name = "pushDeviceId",value = "推送设备id",paramType = "query", required = true),
            @ApiImplicitParam(name = "recordId",value = "记录id",paramType = "query", required = true),
            @ApiImplicitParam(name = "version",value = "版本号",paramType = "query", required = true),
            @ApiImplicitParam(name = "wifiName",value = "wifi名",paramType = "query", required = true),
            @ApiImplicitParam(name = "imgSeq",value = "影像流水",paramType = "query", required = true),
            @ApiImplicitParam(name = "similarity",value = "相似度",paramType = "query", required = true),
            @ApiImplicitParam(name = "failReason",value = "失败原因",paramType = "query", required = true),
            @ApiImplicitParam(name = "remarks1",value = "备注",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate",value = "修改时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "登录来源 2-移动端 3-后台管理", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "登录方式，传统(密码登录)   人脸登录",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "token信息",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段6",paramType = "query"),
            @ApiImplicitParam(name = "ext7",value = "扩展字段7",paramType = "query"),
            @ApiImplicitParam(name = "ext8",value = "扩展字段8",paramType = "query"),
            @ApiImplicitParam(name = "ext9",value = "扩展字段9",paramType = "query"),
    })
    public Map<String,Object> insertIbsaccessLogForLogin(@RequestBody Map<String,String> paramMap) {
        return sysIbsaccessLogForLoginService.insertIbsaccessLogForLogin(paramMap);
    }

    @PostMapping("/updateIbsaccessLogForLogin")
    @ApiOperation(value = "修改登录日志", notes = "修改登录日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "userId",value = "用户编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptId",value = "用户机构编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "address",value = "地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "bankType",value = "银行类型",paramType = "query"),
            @ApiImplicitParam(name = "clientNo",value = "客户编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deviceId",value = "设备编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deviceType",value = "设备类型",paramType = "query"),
            @ApiImplicitParam(name = "loginState",value = "登录状态 1-成功 2-失败 3-首次登陆",paramType = "query", required = true),
            @ApiImplicitParam(name = "latitude",value = "纬度",paramType = "query", required = true),
            @ApiImplicitParam(name = "longitude",value = "经度",paramType = "query", required = true),
            @ApiImplicitParam(name = "loginDate",value = "登录时间",paramType = "query"),
            @ApiImplicitParam(name = "logoutDate",value = "登出时间",paramType = "query"),
            @ApiImplicitParam(name = "network",value = "网络",paramType = "query"),
            @ApiImplicitParam(name = "pushDeviceId",value = "推送设备id",paramType = "query", required = true),
            @ApiImplicitParam(name = "recordId",value = "记录id",paramType = "query", required = true),
            @ApiImplicitParam(name = "version",value = "版本号",paramType = "query", required = true),
            @ApiImplicitParam(name = "wifiName",value = "wifi名",paramType = "query", required = true),
            @ApiImplicitParam(name = "imgSeq",value = "影像流水",paramType = "query", required = true),
            @ApiImplicitParam(name = "similarity",value = "相似度",paramType = "query", required = true),
            @ApiImplicitParam(name = "failReason",value = "失败原因",paramType = "query", required = true),
            @ApiImplicitParam(name = "remarks1",value = "备注",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate",value = "修改时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "登录来源 2-移动端 3-后台管理", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "登录方式，传统(密码登录)   人脸登录",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "token信息",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段6",paramType = "query"),
            @ApiImplicitParam(name = "ext7",value = "扩展字段7",paramType = "query"),
            @ApiImplicitParam(name = "ext8",value = "扩展字段8",paramType = "query"),
            @ApiImplicitParam(name = "ext9",value = "扩展字段9",paramType = "query"),
    })
    public Map<String,Object> updateIbsaccessLogForLogin(@RequestBody Map<String,String> paramMap) {
        return sysIbsaccessLogForLoginService.updateIbsaccessLogForLogin(paramMap);
    }

    @PostMapping("/deleteIbsaccessLogForLogin")
    @ApiOperation(value = "删除登录日志", notes = "删除登录日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteIbsaccessLogForLogin(@RequestBody Map<String,String> paramMap) {
        return sysIbsaccessLogForLoginService.deleteIbsaccessLogForLogin(paramMap);
    }
}
