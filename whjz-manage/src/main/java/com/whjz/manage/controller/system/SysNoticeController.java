package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysNoticeService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：通知公告相关接口
 */

@Api(tags = {"通知公告管理"})
@RestController
@Slf4j
@RequestMapping("sysNotice")
public class SysNoticeController extends AbstractControllerImpl {

    @Autowired
    private SysNoticeService sysNoticeService;

    @PostMapping("/queryNoticeList")
    @ApiOperation(value = "查询通知公告列表", notes = "根据查询条件返回通知公告列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "noticeTitle",value = "通知标题  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "noticeType",value = "通知类别 1-角色级 2-账号级 3-平台级  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "noticeRange",value = "通知范围 1角色级：下属角色 2账号级，针对账号账号3-平台级针对平台  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "noticeState",value = "状态 1-启用，2-停用  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "noticeImportance",value = "通知公告重要程度 1-正常 2-紧急  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryNoticeList(@RequestBody Map<String,String> paramMap) {
        return sysNoticeService.queryNoticeList(paramMap);
    }

    @PostMapping("/insertNotice")
    @ApiOperation(value = "添加通知公告", notes = "添加通知公告")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeCode",value = "通知公告编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeTitle",value = "通知公告标题",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeContent",value = "通知公告内容",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeType",value = "通知公告类别 1-角色级 2-账号级 3-平台级",paramType = "query"),
            @ApiImplicitParam(name = "noticeRange",value = "通知公告范围 1角色级：下属角色 2账号级，针对账号账号3-平台级针对平台",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeState",value = "通知公告状态 1-启用，2-停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeImportance",value = "通知公告重要程度 1-正常 2-紧急",paramType = "query"),
            @ApiImplicitParam(name = "startDate",value = "开始时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "endDate",value = "结束时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser",value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertNotice(@RequestBody Map<String,String> paramMap) {
        return sysNoticeService.insertNotice(paramMap);
    }

    @PostMapping("/updateNotice")
    @ApiOperation(value = "修改通知公告", notes = "修改通知公告")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeCode",value = "通知公告编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeTitle",value = "通知公告标题",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeContent",value = "通知公告内容",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeType",value = "通知公告类别 1-角色级 2-账号级 3-平台级",paramType = "query"),
            @ApiImplicitParam(name = "noticeRange",value = "通知公告范围 1角色级：下属角色 2账号级，针对账号账号3-平台级针对平台",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeState",value = "通知公告状态 1-启用，2-停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "noticeImportance",value = "通知公告重要程度 1-正常 2-紧急",paramType = "query"),
            @ApiImplicitParam(name = "startDate",value = "开始时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "endDate",value = "结束时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser",value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateNotice(@RequestBody Map<String,String> paramMap) {
        return sysNoticeService.updateNotice(paramMap);
    }

    @PostMapping("/deleteNotice")
    @ApiOperation(value = "删除通知公告", notes = "删除通知公告")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteNotice(@RequestBody Map<String,String> paramMap) {
        return sysNoticeService.deleteNotice(paramMap);
    }
}
