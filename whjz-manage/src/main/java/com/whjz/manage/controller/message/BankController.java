package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.BankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行管理类
 */
@Api(tags = "银行管理")
@Slf4j
@RestController
@RequestMapping("bank")
public class BankController {
    @Autowired
    @SuppressWarnings("all")
    private BankService bankService;

    @PostMapping("/queryBankList")
    @ApiOperation(value = "查询银行管理列表", notes = "根据条件银行管理返回银行管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "code", value = "银行编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "name", value = "银行名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryBankList(@RequestBody Map<String, String> paramMap) {

        return bankService.queryBankList(paramMap);
    }

    @PostMapping("/insertBank")
    @ApiOperation(value = "添加银行管理列表", notes = "添加银行管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "code", value = "银行编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "name", value = "银行名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertBank(@RequestBody Map<String, String> ParamMap) {
        return bankService.insertBank(ParamMap);
    }

    @PostMapping("/updateBank")
    @ApiOperation(value = "修改银行管理列表", notes = "修改银行管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "code", value = "银行编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "name", value = "银行名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateBank(@RequestBody Map<String, String> ParamMap) {
        return bankService.updateBank(ParamMap);
    }

    @PostMapping("/deleteBank")
    @ApiOperation(value = "删除银行管理列表", notes = "删除银行管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteBank(@RequestBody Map<String, String> paramMap) {
        return bankService.deleteBank(paramMap);
    }
}

