package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.BankCardService;
import com.whjz.manage.service.message.StorageRoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:储物间类
 */
@Api(tags = "储物间")
@Slf4j
@RestController
@RequestMapping("StorageRoom")
public class StorageRoomController {
    @Autowired
    @SuppressWarnings("all")
    private StorageRoomService storageRoomService;


    @PostMapping("/queryStorageRoomList")
    @ApiOperation(value = "查询储物间列表", notes = "根据条件储物间返回储物间列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productName", value = "产品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productType", value = "产品分类", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "商家ID", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "商家名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "crazyRequisitionDate", value = "疯狂请购时间", paramType = "query"),
            @ApiImplicitParam(name = "stock", value = "库存", paramType = "query"),
            @ApiImplicitParam(name = "productDetails", value = "商品详情", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryStorageRoomList(@RequestBody Map<String, String> paramMap) {

        return storageRoomService.queryStorageRoomList(paramMap);

    }
    @PostMapping("/insertStorageRoom")
    @ApiOperation(value = "添加储物间列表", notes = "添加储物间列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productName", value = "产品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productType", value = "产品分类", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "商家ID", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "商家名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "crazyRequisitionDate", value = "疯狂请购时间", paramType = "query"),
            @ApiImplicitParam(name = "stock", value = "库存", paramType = "query"),
            @ApiImplicitParam(name = "productDetails", value = "商品详情", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertStorageRoom(@RequestBody Map<String, String> paramMap) {

        return storageRoomService.insertStorageRoom(paramMap);

    }
    @PostMapping("/updateStorageRoom")
    @ApiOperation(value = "修改储物间列表", notes = "修改储物间列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productName", value = "产品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productType", value = "产品分类", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "商家ID", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "商家名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "crazyRequisitionDate", value = "疯狂请购时间", paramType = "query"),
            @ApiImplicitParam(name = "stock", value = "库存", paramType = "query"),
            @ApiImplicitParam(name = "productDetails", value = "商品详情", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateStorageRoom(@RequestBody Map<String, String> paramMap) {

        return storageRoomService.updateStorageRoom(paramMap);

    }
    @PostMapping("/deleteStorageRoom")
    @ApiOperation(value = "删除储货间列表", notes = "删除储货间列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteStorageRoom(@RequestBody Map<String, String> paramMap) {
        return storageRoomService.deleteStorageRoom(paramMap);
    }
}
