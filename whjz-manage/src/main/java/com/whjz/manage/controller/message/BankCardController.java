package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.BankCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行管理类
 */
@Api(tags = "银行卡管理")
@Slf4j
@RestController
@RequestMapping("bankCard")
public class BankCardController {
    @Autowired
    @SuppressWarnings("all")
    private BankCardService bankCardService;


    @PostMapping("/queryBankCardList")
    @ApiOperation(value = "查询银行卡管理列表", notes = "根据条件银行卡管理返回银行卡管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBack", value = "开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBackAddress", value = "开户网点", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardNumber", value = "卡号码", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardName", value = "户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardPhone", value = "预留手机号", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryBankCardList(@RequestBody Map<String, String> paramMap) {

        return bankCardService.queryBankCardList(paramMap);

    }
    @PostMapping("/insertBankCard")
    @ApiOperation(value = "添加银行卡管理列表", notes = "添加银行卡管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBack", value = "开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBackAddress", value = "开户网点", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardNumber", value = "卡号码", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardName", value = "户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardPhone", value = "预留手机号", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertBankCard(@RequestBody Map<String, String> ParamMap) {
        return bankCardService.insertBankCard(ParamMap);
    }

    @PostMapping("/updateBankCard")
    @ApiOperation(value = "修改银行卡管理列表", notes = "修改银行卡管理列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBack", value = "开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "openBackAddress", value = "开户网点", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardNumber", value = "卡号码", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardName", value = "户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "bankCardPhone", value = "预留手机号", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateBankCard(@RequestBody Map<String, String> ParamMap) {
        return bankCardService.updateBankCard(ParamMap);
    }

    @PostMapping("/deleteBankCard")
    @ApiOperation(value = "删除银行卡管理列表", notes = "删除银行卡管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteBankCard(@RequestBody Map<String, String> paramMap) {
        return bankCardService.deleteBankCard(paramMap);
    }
}