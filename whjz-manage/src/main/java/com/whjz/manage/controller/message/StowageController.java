package com.whjz.manage.controller.message;


import com.whjz.manage.service.message.StowageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:配载单管理类
 */
@Api(tags = "配载单管理")
@Slf4j
@RestController
@RequestMapping("stowage")
public class StowageController {
    @Autowired
    @SuppressWarnings("all")
    private StowageService stowageService;

    @PostMapping("/queryStowageList")
    @ApiOperation(value = "查询配载单管理列表", notes = "根据条件配载单管理返回配载单管理列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "stowageCode", value = "运载单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverCode", value = "司机编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverName", value = "司机名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverLicense", value = "驾驶证编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "licenseNumber", value = "车牌号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bearingCapacity", value = "承载量", paramType = "query"),
            @ApiImplicitParam(name = "frequency", value = "班次", paramType = "query"),
            @ApiImplicitParam(name = "sumCopiesNumber", value = "合计份数", paramType = "query"),
            @ApiImplicitParam(name = "serialNumber", value = "配载单流水号", paramType = "query"),
            @ApiImplicitParam(name = "stowageState", value = "状态 1-商家备货中2-司机送货中3-驿站已签收", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })

    public Map<String, Object> queryStowageList(@RequestBody Map<String, String> paramMap) {

        return stowageService.queryStowageList(paramMap);

    }
    @PostMapping("/insertStowage")
    @ApiOperation(value = "添加载单管理列表", notes = "添加载单管理列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "stowageCode", value = "运载单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverCode", value = "司机编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverName", value = "司机名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverLicense", value = "驾驶证编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "licenseNumber", value = "车牌号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bearingCapacity", value = "承载量", paramType = "query"),
            @ApiImplicitParam(name = "frequency", value = "班次", paramType = "query"),
            @ApiImplicitParam(name = "sumCopiesNumber", value = "合计份数", paramType = "query"),
            @ApiImplicitParam(name = "serialNumber", value = "配载单流水号", paramType = "query"),
            @ApiImplicitParam(name = "stowageState", value = "状态 1-商家备货中2-司机送货中3-驿站已签收", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertStowage(@RequestBody Map<String, String> paramMap) {

        return stowageService.insertStowage(paramMap);

    }
    @PostMapping("/updateStowage")
    @ApiOperation(value = "修改载单管理列表", notes = "修改载单管理列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "stowageCode", value = "运载单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverCode", value = "司机编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverName", value = "司机名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "driverLicense", value = "驾驶证编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "licenseNumber", value = "车牌号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bearingCapacity", value = "承载量", paramType = "query"),
            @ApiImplicitParam(name = "frequency", value = "班次", paramType = "query"),
            @ApiImplicitParam(name = "sumCopiesNumber", value = "合计份数", paramType = "query"),
            @ApiImplicitParam(name = "serialNumber", value = "配载单流水号", paramType = "query"),
            @ApiImplicitParam(name = "stowageState", value = "状态 1-商家备货中2-司机送货中3-驿站已签收", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateStowage(@RequestBody Map<String, String> paramMap) {

        return stowageService.updateStowage(paramMap);

    }
    @PostMapping("/deleteStowage")
    @ApiOperation(value = "删除载单管理列表", notes = "删除载单管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteStowage(@RequestBody Map<String, String> paramMap) {
        return stowageService.deleteStowage(paramMap);
    }
}