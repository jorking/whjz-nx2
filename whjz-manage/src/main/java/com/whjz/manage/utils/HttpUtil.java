package com.whjz.manage.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class HttpUtil {
	/**
	 * 发送Post请求
	 * @param url
	 * @param parameters
	 * @return
	 */
	public static String sendPost(String url, Map<String,Object> parameters) {
		String result="";
		BufferedReader in =null;
		PrintWriter out =null;
		StringBuffer sb = new StringBuffer();
		String params= "";
		try {
			if(parameters.size() == 1) {
				for (String name : parameters.keySet()) {
						sb.append(name).append("=").append(URLEncoder.encode((String) parameters.get(name), "UTF-8"));
				}
			}else {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(URLEncoder.encode((String) parameters.get(name), "UTF-8"))
					.append("&");
				}
				String temp_params = sb.toString();
				params = temp_params.substring(0, temp_params.length()-1);
			}
			// 创建URL对象
			URL connURL = new URL(url);
			// 打开URL连接
			HttpURLConnection httpConn=(HttpURLConnection) connURL.openConnection();
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			httpConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			httpConn.setConnectTimeout(20*1000); // 毫秒  20秒超时
			httpConn.setReadTimeout(20*1000); // 毫秒 20秒超时
			httpConn.setRequestMethod("POST");
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);
			
			out = new PrintWriter(httpConn.getOutputStream());
			out.write(params);
			out.flush();
			in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
			String line;
			while((line = in.readLine()) != null) {
				result += line;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(out != null) {
					out.close();
				}
				if(in != null) {
					in.close();
				}
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	
	
	
}
