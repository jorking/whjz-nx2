package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysUser;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户相关接口
 */

public interface SysUserMapper {
    //查询用户
    List<SysUser> queryUserList(Map<String, String> paramMap);

    //添加用户
    boolean insertUser(Map<String, String> paramMap);

    //根据ID查询用户
    SysUser queryUserById(@Param("autoId") String autoId);

    //修改用户
    boolean modifyUser(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryUserCount(Map<String, String> paramMap);
}
