package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysRequestLog;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-24
 * remarks：日志相关接口
 */

public interface SysRequestLogMapper {
    //查询字典类别
    List<SysRequestLog> queryRequestLogList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertRequestLog(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyRequestLog(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteRequestLog(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryRequestLogCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysRequestLog queryRequestLogById(@Param("autoId") String autoId);
}
