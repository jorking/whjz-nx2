package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysUserOnline;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构相关接口
 */

public interface SysUserOnlineMapper {
    //查询字典类别
    List<SysUserOnline> queryUserOnlineList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertUserOnline(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyUserOnline(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteUserOnline(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryUserOnlineCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysUserOnline queryUserOnlineById(@Param("autoId") String autoId);
}
