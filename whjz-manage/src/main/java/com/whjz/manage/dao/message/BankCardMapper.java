package com.whjz.manage.dao.message;

import com.whjz.entity.message.BankCard;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行卡管理接口
 */
public interface BankCardMapper {
    //查询银行卡管理
    List<BankCard> queryBankCardList(Map<String, String> paramMap);
    //增加银行卡管理
    boolean insertBankCard(Map<String,String >paraMap);
    //修改银行卡管理
    boolean updateBankCard(Map<String, String> parMap);
    //删除银行卡管理
    boolean deleteBankCard(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryBankCardCount(Map<String,String> paramMap);
}
