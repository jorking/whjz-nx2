package com.whjz.manage.dao.message;

import com.whjz.entity.message.Order;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：订单相关接口
 */

public interface OrderMapper {
    //查询合同
    List<Order> queryOrderList(Map<String, String> paramMap);

    //添加合同
    boolean insertOrder(Map<String, String> paramMap);

    //根据ID查询合同
    Order queryOrderById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyOrder(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryOrderCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deleteOrder(@Param("autoId") String autoId);
}
