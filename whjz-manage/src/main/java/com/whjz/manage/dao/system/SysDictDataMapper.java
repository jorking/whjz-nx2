package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysDictData;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典类别相关接口
 */

public interface SysDictDataMapper {
    //查询字典类别
    List<SysDictData> queryDictDataList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertDictData(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyDictData(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteDictData(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryDictDataCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysDictData queryDictDataById(@Param("autoId") String autoId);
}
