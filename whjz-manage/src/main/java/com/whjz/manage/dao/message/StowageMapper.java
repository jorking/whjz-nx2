package com.whjz.manage.dao.message;

import com.whjz.entity.message.Stowage;
import feign.Param;

import java.util.List;
import java.util.Map;
/**
 * author:hj
 * date:2020-1013
 * remarks;配载单管理接口
 */
public interface StowageMapper {


    //查询配载单管理
    List<Stowage> queryStowageList(Map<String, String> paramMap);
    //增加配载单管理
    boolean insertStowage(Map<String,String >paraMap);
    //修改配载单管理
    boolean updateStowage(Map<String, String> parMap);
    //删除配载单管理
    boolean deleteStowage (@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryStowageCount (Map<String,String> paramMap);
}
