package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysRoleMenu;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：角色菜单相关接口
 */

public interface SysRoleMenuMapper {
    //查询字典类别
    List<SysRoleMenu> queryRoleMenuList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertRoleMenu(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyRoleMenu(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteRoleMenu(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryRoleMenuCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysRoleMenu queryRoleMenuById(@Param("autoId") String autoId);
}
