package com.whjz.manage.dao.message;

import com.whjz.entity.message.Product;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品相关接口
 */

public interface ProductMapper {
    //查询合同
    List<Product> queryProductList(Map<String, String> paramMap);

    //添加合同
    boolean insertProduct(Map<String, String> paramMap);

    //根据ID查询合同
    Product queryProductById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyProduct(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryProductCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deleteProduct(@Param("autoId") String autoId);
}
