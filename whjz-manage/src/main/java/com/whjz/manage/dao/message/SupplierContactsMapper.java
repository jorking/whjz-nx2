package com.whjz.manage.dao.message;

import com.whjz.entity.message.SupplierContacts;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 *  author: hj
 *  date: 2020-10-6
 *  remarks：供应商联系人相关接口
 */

public interface SupplierContactsMapper {


    //查询供应商联系人管理
    List<SupplierContacts> querySupplierContactsList(Map<String, String> paramMap);
    //增加供应商联系人管理
    boolean insertSupplierContacts(Map<String,String >paraMap);
    //修改供应商联系人管理
     boolean updateSupplierContacts(Map<String, String> parMap);
    //删除供应商联系人管理
    boolean deleteSupplierContacts(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int querySupplierContactsCount(Map<String, String> paramMap);

}
