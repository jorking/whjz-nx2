package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysNotice;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-
 * remarks：相关接口
 */

public interface SysNoticeMapper {
    //查询字典类别
    List<SysNotice> queryNoticeList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertNotice(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyNotice(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteNotice(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryNoticeCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysNotice queryNoticeById(@Param("autoId") String autoId);
}
