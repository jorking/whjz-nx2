package com.whjz.manage.dao.message;

import com.whjz.entity.message.Evaluate;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：评价相关接口
 */

public interface EvaluateMapper {
    //查询合同
    List<Evaluate> queryEvaluateList(Map<String, String> paramMap);

    //添加合同
    boolean insertEvaluate(Map<String, String> paramMap);

    //根据ID查询合同
    Evaluate queryEvaluateById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyEvaluate(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryEvaluateCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deleteEvaluate(@Param("autoId") String autoId);
}
