package com.whjz.manage.dao.message;

import com.whjz.entity.message.CommonProblem;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 *  author: hj
 *  date: 2020-10-11
 *  remarks：常见问题相关接口
 */
public interface CommonProblemMapper {
    //查询常见问题
    List<CommonProblem> queryCommonProblemList(Map<String, String> paramMap);
    //增加常见问题
    boolean insertCommonProblem(Map<String,String >paraMap);
    //修改常见问题
    boolean updateCommonProblem(Map<String, String> parMap);
    //删除常见问题
    boolean deleteCommonProblem(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryCommonProblemCount(Map<String,String> paramMap);

}
