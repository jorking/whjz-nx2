package com.whjz.manage.dao.message;

import com.whjz.entity.message.ProductType;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品分类相关接口
 */

public interface ProductTypeMapper {
    //查询产品分类
    List<ProductType> queryProductTypeList(Map<String, String> paramMap);

    //添加产品分类
    boolean insertProductType(Map<String, String> paramMap);

    //根据ID查询产品分类
    ProductType queryProductTypeById(@Param("autoId") String autoId);

    //修改产品分类
    boolean modifyProductType(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryProductTypeCount(Map<String, String> paramMap);

    //删除产品分类管理
    boolean deleteProductType(@Param("autoId") String autoId);
}
