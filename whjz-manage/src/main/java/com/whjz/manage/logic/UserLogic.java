package com.whjz.manage.logic;

import com.whjz.utils.MD5Util;

/**
 * author: fln
 * date: 2020-09-27
 * remarks：用户处理业务逻辑类
 */

public class UserLogic {

    //MD5加密方法
    public static String MD5(String userPasswd) {
        String newPassword = MD5Util.toMD5String(userPasswd);
        return newPassword;
    }
}
