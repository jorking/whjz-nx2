package com.whjz.manage.service.message;

import java.util.Map;
/**
 * author:hj
 * date:2020-10-13
 * remarks:储货间接口
 */
public interface StorageRoomService {

    Map<String, Object> queryStorageRoomList(Map<String, String> paramMap);

    Map<String, Object> insertStorageRoom(Map<String, String> paramMap);
    Map<String, Object> updateStorageRoom(Map<String, String> paramMap);

    Map<String, Object> deleteStorageRoom(Map<String, String> paramMap);


}
