package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:运单信息接口
 */
public interface WaybillService {

    Map<String, Object> queryWaybillList (Map<String, String> paramMap);

    Map<String, Object> insertWaybill (Map<String, String> paramMap);

    Map<String, Object> updateWaybill (Map<String, String> paramMap);

    Map<String, Object> deleteWaybill (Map<String, String> paramMap);
}
