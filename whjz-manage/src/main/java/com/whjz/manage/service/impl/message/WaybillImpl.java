package com.whjz.manage.service.impl.message;
import com.whjz.entity.message.Waybill;
import com.whjz.manage.dao.message.WaybillMapper;
import com.whjz.manage.service.message.WaybillService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:运单信息类
 */
@Slf4j
@Service(value ="waybill" )
@SuppressWarnings("all")
public class WaybillImpl implements WaybillService {
    @Autowired
    private WaybillMapper waybillMapper;
    /**
     *
     * @param paramMap 查询运单信息
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object>queryWaybillList(Map<String, String> paramMap) {

        try {
            List<Waybill> queryWaybill = waybillMapper.queryWaybillList(paramMap);
            int count = waybillMapper.queryWaybillCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, queryWaybill, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
    /**
     *
     * @param paramMap 添加运单信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertWaybill(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = waybillMapper.insertWaybill(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
    /**
     * @param paramMap 修改运单信息
     * @return 修改成功或失败
     */
@Override
    public Map<String, Object> updateWaybill(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = waybillMapper.updateWaybill(paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 删除运单信息
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteWaybill(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    waybillMapper.deleteWaybill(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
