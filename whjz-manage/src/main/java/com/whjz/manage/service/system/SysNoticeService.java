package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-12
 * remarks：通知相关接口
 */

public interface SysNoticeService {

    Map<String, Object> queryNoticeList(Map<String, String> paramMap);

    Map<String, Object> insertNotice(Map<String, String> paramMap);

    Map<String, Object> updateNotice(Map<String, String> paramMap);

    Map<String, Object> deleteNotice(Map<String, String> paramMap);
}
