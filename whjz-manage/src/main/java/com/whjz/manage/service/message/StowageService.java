package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:配载单管理接口
 */

public interface StowageService {

    Map<String, Object> queryStowageList (Map<String, String> paramMap);

    Map<String, Object> insertStowage (Map<String, String> paramMap);

    Map<String, Object> updateStowage (Map<String, String> paramMap);

    Map<String, Object> deleteStowage (Map<String, String> paramMap);
}