package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.SupplierContacts;
import com.whjz.manage.dao.message.SupplierContactsMapper;
import com.whjz.manage.service.message.SupplierContactsService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * author:hj
 * date:2020-10-6
 * remarks:供应商联系人管理类
 */
@Slf4j
@Service(value ="SupplierContacts" )
@SuppressWarnings("all")
public class SupplierContactsImpl implements SupplierContactsService {


    @Autowired
    private SupplierContactsMapper supplierContactsMapper;


    /**
     * @param paramMap 查询供应商联系人
     * @return 查询成功或失败
     */

    @Override
    public Map<String, Object> querySupplierContactsList(Map<String, String> paramMap) {

        try {
            List<SupplierContacts> supplierContactsList = supplierContactsMapper.querySupplierContactsList(paramMap);
            int count = supplierContactsMapper.querySupplierContactsCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, supplierContactsList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 添加供应商联系人管理
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertSupplierContacts(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = supplierContactsMapper.insertSupplierContacts(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 供应商联系人相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateSupplierContacts(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = supplierContactsMapper.updateSupplierContacts (paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 供应商联系人相关信息
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteSupplierContacts(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    supplierContactsMapper.deleteSupplierContacts(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}