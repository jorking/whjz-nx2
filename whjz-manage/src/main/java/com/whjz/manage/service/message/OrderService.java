package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-10
 * remarks：订单相关接口
 */

public interface OrderService {

    Map<String, Object> queryOrderList(Map<String, String> paramMap);

    Map<String, Object> insertOrder(Map<String, String> paramMap);

    Map<String, Object> deleteOrder(Map<String, String> paramMap);

    Map<String, Object> updateOrder(Map<String, String> paramMap);
}
