package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysDeptMapper;
import com.whjz.entity.system.SysDept;
import com.whjz.manage.service.system.SysDeptService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构相关接口
 */
@Slf4j
@Service(value = "sysDeptService")
@SuppressWarnings("all")
public class SysDeptServiceImpl implements SysDeptService {

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     *
     * @param paramMap  查询机构条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryDeptList(Map<String, String> paramMap) {
        try {
            List<SysDept> sysUserList = sysDeptMapper.queryDeptList(paramMap);
            int count = sysDeptMapper.queryDeptCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertDept(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            paramMap.put("state", StringConstantUtil.DEPTSTATESNORMAL);
            boolean flag = sysDeptMapper.insertDept(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateDept(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = sysDeptMapper.modifyDept(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteDept(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysDeptMapper.deleteDept(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
