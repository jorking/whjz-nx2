package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-21
 * remarks：角色菜单相关接口
 */

public interface SysRoleMenuService {

    Map<String, Object> queryRoleMenuList(Map<String, String> paramMap);

    Map<String, Object> insertRoleMenu(Map<String, String> paramMap);

    Map<String, Object> updateRoleMenu(Map<String, String> paramMap);

    Map<String, Object> deleteRoleMenu(Map<String, String> paramMap);
}
