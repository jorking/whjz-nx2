package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典类别相关接口
 */

public interface SysDictTypeService {

    Map<String, Object> queryDictTypeList(Map<String, String> paramMap);

    Map<String, Object> insertDictType(Map<String, String> paramMap);

    Map<String, Object> updateDictType(Map<String, String> paramMap);

    Map<String, Object> deleteDictType(Map<String, String> paramMap);
}
