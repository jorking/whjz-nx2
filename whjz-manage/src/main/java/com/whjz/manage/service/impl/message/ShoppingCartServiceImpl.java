package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.ShoppingCart;
import com.whjz.manage.dao.message.ShoppingCartMapper;
import com.whjz.manage.service.message.ShoppingCartService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：购物车相关接口
 */
@Slf4j
@Service(value = "shoppingCartService")
@SuppressWarnings("all")
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * @param paramMap 查询购物车条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryShoppingCartList(Map<String, String> paramMap) {
        try {
            List<ShoppingCart> shoppingCartList = shoppingCartMapper.queryShoppingCartList(paramMap);
            int shoppingCartCount = shoppingCartMapper.queryShoppingCartCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, shoppingCartList, shoppingCartCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 购物车相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertShoppingCart(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = shoppingCartMapper.insertShoppingCart(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 购物车相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateShoppingCart(Map<String, String> paramMap) {
        try {
            boolean flag = shoppingCartMapper.modifyShoppingCart(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteShoppingCart(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    shoppingCartMapper.deleteShoppingCart(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
