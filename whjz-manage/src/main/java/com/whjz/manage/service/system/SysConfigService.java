package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-12
 * remarks：配置相关接口
 */

public interface SysConfigService {

    Map<String, Object> queryConfigList(Map<String, String> paramMap);

    Map<String, Object> insertConfig(Map<String, String> paramMap);

    Map<String, Object> updateConfig(Map<String, String> paramMap);

    Map<String, Object> deleteConfig(Map<String, String> paramMap);
}
