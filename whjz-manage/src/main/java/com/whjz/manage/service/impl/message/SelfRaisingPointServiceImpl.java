package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.SelfRaisingPoint;
import com.whjz.manage.dao.message.SelfRaisingPointMapper;
import com.whjz.manage.service.message.SelfRaisingPointService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：自提点相关接口
 */
@Slf4j
@Service(value = "SelfRaisingPointService")
@SuppressWarnings("all")
public class SelfRaisingPointServiceImpl implements SelfRaisingPointService {

    @Autowired
    private SelfRaisingPointMapper selfRaisingPointMapper;

    /**
     * @param paramMap 查询自提点条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> querySelfRaisingPointList(Map<String, String> paramMap) {
        try {
            List<SelfRaisingPoint> selfRaisingPointList = selfRaisingPointMapper.querySelfRaisingPointList(paramMap);
            int selfRaisingPointCount = selfRaisingPointMapper.querySelfRaisingPointCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, selfRaisingPointList, selfRaisingPointCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 自提点相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertSelfRaisingPoint(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = selfRaisingPointMapper.insertSelfRaisingPoint(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 自提点相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateSelfRaisingPoint(Map<String, String> paramMap) {
        try {
            boolean flag = selfRaisingPointMapper.modifySelfRaisingPoint(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteSelfRaisingPoint(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    selfRaisingPointMapper.deleteSelfRaisingPoint(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
