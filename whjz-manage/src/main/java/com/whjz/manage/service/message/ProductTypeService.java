package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品分类相关接口
 */

public interface ProductTypeService {

    Map<String, Object> queryProductTypeList(Map<String, String> paramMap);

    Map<String, Object> insertProductType(Map<String, String> paramMap);

    Map<String, Object> deleteProductType(Map<String, String> paramMap);

    Map<String, Object> updateProductType(Map<String, String> paramMap);
}
