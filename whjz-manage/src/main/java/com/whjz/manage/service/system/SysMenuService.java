package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-25
 * remarks：菜单相关接口
 */

public interface SysMenuService {

    Map<String, Object> queryMenuList(Map<String, String> paramMap);

    Map<String, Object> insertMenu(Map<String, String> paramMap);

    Map<String, Object> updateMenu(Map<String, String> paramMap);

    Map<String, Object> deleteMenu(Map<String, String> paramMap);
}
