package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-12
 * remarks：机构类别相关接口
 */

public interface SysUserRoleService {

    Map<String, Object> queryUserRoleList(Map<String, String> paramMap);

    Map<String, Object> insertUserRole(Map<String, String> paramMap);

    Map<String, Object> updateUserRole(Map<String, String> paramMap);

    Map<String, Object> deleteUserRole(Map<String, String> paramMap);
}
