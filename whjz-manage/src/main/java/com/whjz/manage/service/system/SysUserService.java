package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户相关接口
 */

public interface SysUserService {

    Map<String, Object> queryUserList(Map<String, String> paramMap);

    Map<String, Object> insertUser(Map<String, String> paramMap);

    Map<String, Object> deleteUser(Map<String, String> paramMap);

    Map<String, Object> updateUser(Map<String, String> paramMap);

    Map<String, Object> frozenUser(Map<String, String> paramMap);

    Map<String, Object> resetUserPwd(Map<String, String> paramMap);
}
