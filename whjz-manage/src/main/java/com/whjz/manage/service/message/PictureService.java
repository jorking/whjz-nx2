package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：图片相关接口
 */

public interface PictureService {

    Map<String, Object> queryPictureList(Map<String, String> paramMap);

    Map<String, Object> insertPicture(Map<String, String> paramMap);

    Map<String, Object> deletePicture(Map<String, String> paramMap);

    Map<String, Object> updatePicture(Map<String, String> paramMap);
}
