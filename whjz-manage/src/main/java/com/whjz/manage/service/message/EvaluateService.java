package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：评价相关接口
 */

public interface EvaluateService {

    Map<String, Object> queryEvaluateList(Map<String, String> paramMap);

    Map<String, Object> insertEvaluate(Map<String, String> paramMap);

    Map<String, Object> deleteEvaluate(Map<String, String> paramMap);

    Map<String, Object> updateEvaluate(Map<String, String> paramMap);
}
